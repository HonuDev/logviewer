﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using Honu.LogViewer.App.Messages;
using Honu.LogViewer.App.Properties;
using Honu.LogViewer.App.Views;
using log4net;
using Microsoft.Win32;

namespace Honu.LogViewer.App
{
    /// <summary>
    /// Interaction logic for <see cref="App"/>
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// <see cref="App"/> logger
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(App));

        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            Messenger.Default.Register<RequestOpenFileDialogMessage>(this, this.DoOpenFileDialog);
            Messenger.Default.Register<ShowAboutMessage>(this, this.DoShowAboutMessage);
            Messenger.Default.Register<ShowSettingsMessage>(this, this.DoShowSettingsMessage);
            Messenger.Default.Register<ShowFilterMessage>(this, this.DoShowFilterMessage);
            Messenger.Default.Register<CloseWindowMessage>(this, this.DoCloseWindowMessage);
            Messenger.Default.Register<RequestGoToLogDialogMessage>(this, this.DoRequestGoToLogMessage);
        }

        /// <summary>
        /// Performs the Open File Dialog message.
        /// </summary>
        /// <param name="msg">The message.</param>
        private void DoOpenFileDialog(RequestOpenFileDialogMessage msg)
        {
            string initialDirectory = Settings.Default.InitialDirectory;

            try
            {
                DirectoryInfo dir = new DirectoryInfo(initialDirectory);
                if (!dir.Exists)
                { initialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); }
            }
            catch
            {
                // Default to My Documents path
                initialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            }

            OpenFileDialog dialog = new OpenFileDialog
            {
                Filter = @"XML Files (*.xml)|*.xml|All files (*.*)|*.*",
                CheckFileExists = true,
                Multiselect = true,
                InitialDirectory = initialDirectory
            };

            if (dialog.ShowDialog().GetValueOrDefault(false))
            {
                // Save initial directory
                if (dialog.FileNames != null && dialog.FileNames.Length > 0)
                {
                    FileInfo file = new FileInfo(dialog.FileNames.First());
                    Settings.Default.InitialDirectory = file.DirectoryName;

                    Log.InfoFormat("Setting default Open File directory to: {0}", file.DirectoryName);
                    Settings.Default.Save();
                }

                Messenger.Default.Send(new OpenFilesMessage(dialog.FileNames));
            }
        }

        /// <summary>
        /// Shows the About window.
        /// </summary>
        /// <param name="msg">The message.</param>
        private void DoShowAboutMessage(ShowAboutMessage msg)
        { (new AboutView { Owner = this.MainWindow }).ShowDialog(); }

        /// <summary>
        /// Shows the Settings window.
        /// </summary>
        /// <param name="msg">The message.</param>
        private void DoShowSettingsMessage(ShowSettingsMessage msg)
        { (new SettingsView { Owner = this.MainWindow }).ShowDialog(); }

        /// <summary>
        /// Shows the Filter window.
        /// </summary>
        /// <param name="msg">The message.</param>
        private void DoShowFilterMessage(ShowFilterMessage msg)
        { (new FilterView { Owner = this.MainWindow }).ShowDialog(); }

        /// <summary>
        /// Shows the Go To Log window.
        /// </summary>
        /// <param name="msg">The message.</param>
        private void DoRequestGoToLogMessage(RequestGoToLogDialogMessage msg)
        {
            // Ensure good message
            if (msg == null || msg.Content == null)
            { return; }

            // Cannot go to logs which aren't in the view
            if (msg.Content.Count() == 0)
            { return; }

            (new GoToLogView(msg.Content) { Owner = this.MainWindow }).ShowDialog();
        }

        /// <summary>
        /// Closes the specified window.
        /// </summary>
        /// <param name="msg">The message containing the handle to the window which should be closed.</param>
        private void DoCloseWindowMessage(CloseWindowMessage msg)
        {
            if (msg == null || msg.Content == null)
            { return; }

            msg.Content.Close();
        }
    }
}
