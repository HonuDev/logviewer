﻿using System.Windows.Controls;
using System.Windows.Interactivity;

namespace Honu.LogViewer.App.Behaviors
{
    /// <summary>
    /// Scroll Into View Behavior
    /// </summary>
    /// <seealso cref="System.Windows.Interactivity.Behavior{System.Windows.Controls.ListView}" />
    public sealed class ScrollIntoViewBehavior : Behavior<ListView>
    {
        /// <summary>
        /// Called when Attached.
        /// </summary>
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.SelectionChanged += this.ScrollIntoView;
        }

        /// <summary>
        /// Called when Detaching.
        /// </summary>
        protected override void OnDetaching()
        {
            AssociatedObject.SelectionChanged -= this.ScrollIntoView;
            base.OnDetaching();
        }

        /// <summary>
        /// Scrolls into view.
        /// </summary>
        /// <param name="o">The object.</param>
        /// <param name="e">The <see cref="SelectionChangedEventArgs"/> instance containing the event data.</param>
        private void ScrollIntoView(object o, SelectionChangedEventArgs e)
        {
            ListView v = o as ListView;
            if (v == null || v.SelectedItem == null)
            { return; }

            if (v.Items.Contains(v.SelectedItem))
            { v.ScrollIntoView(v.SelectedItem); }
        }
    }
}