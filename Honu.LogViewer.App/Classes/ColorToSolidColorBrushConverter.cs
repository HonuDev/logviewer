﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Honu.LogViewer.App
{
    /// <summary>
    /// Color to Brush converter
    /// </summary>
    public class ColorToSolidColorBrushConverter : IValueConverter
    {
        /// <summary>
        /// Converts the <see cref="Color"/> value to a <see cref="SolidColorBrush"/> value
        /// </summary>
        /// <param name="value">The <see cref="Color"/> value to convert</param>
        /// <param name="targetType">The target type</param>
        /// <param name="parameter">The parameter</param>
        /// <param name="culture">Culture info</param>
        /// <returns>The <see cref="SolidColorBrush"/> associated with the specified <see cref="Color"/> value</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // Check for error
            if (value == null)
            { return null; }

            // Convert color
            if (value is Color)
            {
                Color color = (Color)value;
                return new SolidColorBrush(color);
            }

            // Bad type
            Type type = value.GetType();
            throw new InvalidOperationException(string.Format("Unsupported type [{0}]", type.Name));
        }

        /// <summary>
        /// Converts a <see cref="SolidColorBrush"/> value back to a <see cref="Color"/> value
        /// </summary>
        /// <param name="value">The <see cref="SolidColorBrush"/> value to convert</param>
        /// <param name="targetType">The target type</param>
        /// <param name="parameter">The parameter</param>
        /// <param name="culture">Culture info</param>
        /// <returns>The <see cref="Color"/> value associated with the specified <see cref="SolidColorBrush"/></returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            { return null; }

            if (value is SolidColorBrush)
            {
                SolidColorBrush brush = value as SolidColorBrush;
                return brush.Color;
            }

            return null;
        }
    }
}
