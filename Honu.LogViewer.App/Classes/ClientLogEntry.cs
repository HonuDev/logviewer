﻿using System;

namespace Honu.LogViewer.App
{
    /// <summary>
    /// Client Log Entry
    /// </summary>
    /// <seealso cref="Honu.LogViewer.LogEntry" />
    internal class ClientLogEntry : LogEntry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClientLogEntry"/> class.
        /// </summary>
        /// <param name="entry">The entry.</param>
        /// <param name="id">The identifier.</param>
        public ClientLogEntry(ILogEntry entry, int id)
        {
            if (entry == null)
            { throw new ArgumentNullException("entry"); }

            this.ID = id;
            this.Application = entry.Application;
            this.Class = entry.Class;
            this.CodeFile = entry.CodeFile;
            this.Host = entry.Host;
            this.Level = entry.Level;
            this.Line = entry.Line;
            this.LogFile = entry.LogFile;
            this.Logger = entry.Logger;
            this.Machine = entry.Machine;
            this.Message = entry.Message;
            this.Method = entry.Method;
            this.Thread = entry.Thread;
            this.Throwable = entry.Throwable;
            this.TimeStamp = entry.TimeStamp;
            this.User = entry.User;
        }

        /// <summary>
        /// Gets the identifier.
        /// </summary>
        public int ID
        { get; private set; }
    }
}