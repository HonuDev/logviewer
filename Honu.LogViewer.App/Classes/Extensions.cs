﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Honu.LogViewer.App
{
    /// <summary>
    /// Application Extensions
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Raises the specified event.
        /// </summary>
        /// <typeparam name="TEventArgs">The type of the event arguments.</typeparam>
        /// <param name="handler">The handler.</param>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="TEventArgs"/> instance containing the event data.</param>
        public static void Raise<TEventArgs>(this EventHandler<TEventArgs> handler, object sender, TEventArgs e)
            where TEventArgs : EventArgs
        {
            if (handler != null)
            { handler(sender, e); }
        }

        /// <summary>
        /// Flattens the collection to a collection of a single depth.
        /// </summary>
        /// <typeparam name="T">The type of collection to be flattened.</typeparam>
        /// <param name="source">The <see cref="IEnumerable{T}"/> providing the collection.</param>
        /// <param name="selector">Child property selector of each <paramref name="source"/> item.</param>
        /// <returns>A single level list of elements within the collection.</returns>
        public static IEnumerable<T> Flatten<T>(this IEnumerable<T> source, Func<T, IEnumerable<T>> selector)
        {
            return source.Flatten((item, coll) => selector(item));
        }

        /// <summary>
        /// Flattens the collection to a collection of a single depth.
        /// </summary>
        /// <typeparam name="T">The type of collection to be flattened.</typeparam>
        /// <param name="source">The <see cref="IEnumerable{T}"/> providing the collection.</param>
        /// <param name="selector">Child property selector of each <paramref name="source"/> item.</param>
        /// <returns>A single level list of elements within the collection.</returns>
        public static IEnumerable<T> Flatten<T>(this IEnumerable<T> source, Func<T, IEnumerable<T>, IEnumerable<T>> selector)
        {
            return source.Concat(
                source
                .Where(item => selector(item, source) != null)
                .SelectMany(item => selector(item, source).Flatten(selector)));
        }

        /// <summary>
        /// Performs the specified action on each element of the <see cref="IEnumerable{T}"/>.
        /// </summary>
        /// <typeparam name="T">The type of object being affected by the <see cref="Action{T}"/>.</typeparam>
        /// <param name="source">The <see cref="IEnumerable{T}"/> providing the collection.</param>
        /// <param name="action">The <see cref="Action{T}"/> to be performed on each element of the collection.</param>
        /// <remarks>Not sure why this is necessary. Can chain ToList() to IEnumerable{T} and access ForEach() built in.</remarks>
        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            if (source == null)
            { throw new ArgumentNullException("source"); }
            if (action == null)
            { throw new ArgumentNullException("action"); }

            foreach (T element in source)
            { action(element); }
        }

        /// <summary>
        /// Performs the specified action on each element of the <see cref="IEnumerable{T}"/>. Maintains a running count of affected elements within the
        /// <see cref="Action{Int32,T}"/>
        /// </summary>
        /// <typeparam name="T">The type of object being affected by the <see cref="Action{Int32,T}"/>.</typeparam>
        /// <param name="source">The <see cref="IEnumerable{T}"/> providing the collection.</param>
        /// <param name="action">The <see cref="Action{Int32,T}"/> to be performed on each element of the collection.</param>
        /// <returns>The number of elements affected by this method.</returns>
        public static int ForEach<T>(this IEnumerable<T> source, Action<int, T> action)
        {
            if (source == null)
            { throw new ArgumentNullException("source"); }
            if (action == null)
            { throw new ArgumentNullException("action"); }

            int idx = 0;
            foreach (T element in source)
            { action(idx++, element); }

            return idx;
        }
    }
}
