﻿using System.Linq;
using System.Reflection;

namespace Honu.LogViewer.App
{
    /// <summary>
    /// Application Attributes
    /// </summary>
    internal static class ApplicationAttributes
    {
        /// <summary>
        /// Initializes static members of the <see cref="ApplicationAttributes"/> class.
        /// </summary>
        static ApplicationAttributes()
        {
            Assembly assm = Assembly.GetEntryAssembly();

            object[] attributes = assm.GetCustomAttributes(false);
            AssemblyCompanyAttribute company = attributes.OfType<AssemblyCompanyAttribute>().FirstOrDefault();
            AssemblyProductAttribute product = attributes.OfType<AssemblyProductAttribute>().FirstOrDefault();

            if (company != null && !string.IsNullOrWhiteSpace(company.Company))
            { ApplicationAttributes.CompanyName = company.Company; }

            if (product != null && !string.IsNullOrWhiteSpace(product.Product))
            { ApplicationAttributes.ProductName = product.Product; }

            ApplicationAttributes.Version = assm.GetName().Version.ToString(2);
        }

        /// <summary>
        /// Gets the company name
        /// </summary>
        public static string CompanyName
        { get; private set; }

        /// <summary>
        /// Gets the product name
        /// </summary>
        public static string ProductName
        { get; private set; }

        /// <summary>
        /// Gets the version
        /// </summary>
        public static string Version
        { get; private set; }
    }
}
