﻿using System.Collections.Generic;

namespace Honu.LogViewer.App
{
    /// <summary>
    /// Log File Info
    /// </summary>
    internal class LogFileInfo
    {
        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        public string FilePath
        { get; set; }

        /// <summary>
        /// Gets or sets the logs.
        /// </summary>
        public IEnumerable<ILogEntry> Logs
        { get; set; }
    }
}
