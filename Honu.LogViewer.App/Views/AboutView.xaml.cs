﻿using System.Windows;

namespace Honu.LogViewer.App.Views
{
    /// <summary>
    /// Interaction logic for <see cref="AboutView"/>.
    /// </summary>
    public partial class AboutView : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AboutView"/> class.
        /// </summary>
        public AboutView()
        { this.InitializeComponent(); }
    }
}
