﻿<Window x:Class="Honu.LogViewer.App.Views.MainView"
		x:Name="me"
		xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
		xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
		xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
		xmlns:i="clr-namespace:System.Windows.Interactivity;assembly=System.Windows.Interactivity"
		xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
		xmlns:props="clr-namespace:Honu.LogViewer.App.Properties"
		xmlns:local="clr-namespace:Honu.LogViewer.App"
		xmlns:controls="clr-namespace:Honu.LogViewer.App.Controls"
		xmlns:behaviors="clr-namespace:Honu.LogViewer.App.Behaviors"
        xmlns:views="clr-namespace:Honu.LogViewer.App.Views"
		xmlns:sys="clr-namespace:System;assembly=mscorlib"
		mc:Ignorable="d" 
		Background="{StaticResource WindowBackgroundBrush}"
		Title="{Binding Title, Mode=OneWay}" 
		Height="{Binding Source={x:Static props:Settings.Default}, Path=MainWindowHeight, Mode=TwoWay}"
		Width="{Binding Source={x:Static props:Settings.Default}, Path=MainWindowWidth, Mode=TwoWay}"
		MinHeight="300"
		MinWidth="400"
		d:DesignHeight="450"
		d:DesignWidth="400"
		Icon="/Log Viewer;component/Resources/Skull.ico">

	<Window.DataContext>
		<Binding Source="{StaticResource Locator}" Path="MainModel" />
	</Window.DataContext>

	<Window.Resources>
		<Style TargetType="Label" BasedOn="{StaticResource {x:Type Label}}">
			<Setter Property="VerticalAlignment" Value="Center" />
			<Setter Property="HorizontalAlignment" Value="Center" />
			<Setter Property="Margin" Value="1,0,10,0" />
		</Style>

		<BooleanToVisibilityConverter x:Key="BoolToVisibilityConverter" />
		<local:ColorToSolidColorBrushConverter x:Key="ColorToSolidColorBrushConverter" />
		<Style TargetType="controls:RecentFileMenuItem" BasedOn="{StaticResource {x:Type MenuItem}}" />
	</Window.Resources>

	<Window.InputBindings>
		<KeyBinding Gesture="CTRL+O" Command="{Binding OpenCommand}" />
		<KeyBinding Gesture="CTRL+F" Command="{Binding FilterCommand}" />
		<KeyBinding Gesture="CTRL+G" Command="{Binding RequestGoToLogCommand}" />
	</Window.InputBindings>

	<DockPanel>
		<Menu DockPanel.Dock="Top">
			<MenuItem Header="_File">
				<MenuItem Header="_Open" InputGestureText="CTRL+O" Command="{Binding OpenCommand}" />
				<controls:RecentFileMenuItem Header="_Recent Files" RecentFiles="{Binding RecentFileModel.RecentFiles}" />
				<Separator />
                <MenuItem Header="E_xit" Command="{Binding CloseWindowCommand}" CommandParameter="{Binding Mode=OneWay, RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type views:MainView}}}"/>
			</MenuItem>

			<MenuItem Header="_Refresh" Command="{Binding RefreshCommand}" />
			<MenuItem Header="F_ilter" Command="{Binding FilterCommand}" />
			<MenuItem Header="_Settings" Command="{Binding SettingsCommand}" />
			<MenuItem Header="_About" Command="{Binding AboutCommand}" />
		</Menu>

		<Grid Margin="5">
			<Grid.RowDefinitions>
				<RowDefinition Height="Auto" />
				<RowDefinition />
				<RowDefinition Height="Auto" />
				<RowDefinition />
			</Grid.RowDefinitions>

			<!-- Row 1 - Counts and Text Filter Panel-->
			<StackPanel HorizontalAlignment="Right" Margin="0,0,0,5">
				<StackPanel.Resources>
					<Style TargetType="Image">
						<Setter Property="VerticalAlignment" Value="Center" />
						<Setter Property="Height" Value="16" />
						<Setter Property="Width" Value="16" />
						<Setter Property="Margin" Value="3,0,0,0" />
					</Style>

					<Style TargetType="ToggleButton" BasedOn="{StaticResource {x:Static ToolBar.ToggleButtonStyleKey}}">
						<Setter Property="Margin" Value="0,0,3,0" />
						<Setter Property="IsTabStop" Value="False" />
						<Setter Property="Focusable" Value="False" />
					</Style>

					<Style TargetType="StackPanel">
						<Setter Property="Orientation" Value="Horizontal" />
					</Style>

					<Style TargetType="Button" BasedOn="{StaticResource {x:Static ToolBar.ButtonStyleKey}}">
						<Setter Property="Margin" Value="0,0,3,0" />
						<Setter Property="IsTabStop" Value="False" />
					</Style>
				</StackPanel.Resources>

				<ToggleButton ToolTip="Filter with Fatal Messages" IsChecked="{Binding FilterModel.FatalLevel}">
					<StackPanel>
						<Image Source="/Log Viewer;component/Resources/Skull16.png" />
						<Label Content="{Binding FatalLogCount}" />
					</StackPanel>
				</ToggleButton>

				<ToggleButton ToolTip="Filter with Error Messages" IsChecked="{Binding FilterModel.ErrorLevel}">
					<StackPanel>
						<Image Source="/Log Viewer;component/Resources/Error16.png" />
						<Label Content="{Binding ErrorLogCount}" />
					</StackPanel>
				</ToggleButton>

				<ToggleButton ToolTip="Filter with Warning Messages" IsChecked="{Binding FilterModel.WarnLevel}">
					<StackPanel>
						<Image Source="/Log Viewer;component/Resources/Warn16.png" />
						<Label Content="{Binding WarnLogCount}" />
					</StackPanel>
				</ToggleButton>

				<ToggleButton ToolTip="Filter with Info Messages" IsChecked="{Binding FilterModel.InfoLevel}">
					<StackPanel>
						<Image Source="/Log Viewer;component/Resources/Info16.png" />
						<Label Content="{Binding InfoLogCount}" />
					</StackPanel>
				</ToggleButton>

				<ToggleButton ToolTip="Filter with Debug Messages" IsChecked="{Binding FilterModel.DebugLevel}">
					<StackPanel>
						<Image Source="/Log Viewer;component/Resources/Debug16.png" />
						<Label Content="{Binding DebugLogCount}" />
					</StackPanel>
				</ToggleButton>

				<ToggleButton ToolTip="Filter with Unknown Messages" IsChecked="{Binding FilterModel.UnknownLevel}">
					<StackPanel>
						<Image Source="/Log Viewer;component/Resources/Unknown16.png" />
						<Label Content="{Binding UnknownLogCount}" />
					</StackPanel>
				</ToggleButton>

				<Button Command="{Binding ClearFilterCommand}" ToolTip="Clear Filter" Style="{StaticResource {x:Static ToolBar.ButtonStyleKey}}" BorderThickness="0"
						Visibility="{Binding IsDisplayingFilteredView, Converter={StaticResource BoolToVisibilityConverter}}" HorizontalAlignment="Center">
					<Label Margin="10,0,10,0">
						<TextBlock FontStyle="Italic" Foreground="Orange">
							<Run Text="{Binding FilteredLogCount, Mode=OneWay}" />
							<Run Text="/" />
							<Run Text="{Binding FullLogCount, Mode=OneWay}" />
						</TextBlock>
					</Label>
				</Button>

				<Grid Width="250">
					<Grid.ColumnDefinitions>
						<ColumnDefinition Width="Auto" />
						<ColumnDefinition Width="Auto" />
						<ColumnDefinition />
					</Grid.ColumnDefinitions>

					<Image Source="/Log Viewer;component/Resources/Magnifier.png" />
					<Label Grid.Column="1" HorizontalAlignment="Left" Margin="3,0,5,0" Content="Find:" />
					<TextBox Grid.Column="2" VerticalContentAlignment="Center" Text="{Binding FilterModel.Message, UpdateSourceTrigger=PropertyChanged, TargetNullValue={x:Static sys:String.Empty}}"/>
				</Grid>
			</StackPanel>

			<!-- Row 2 - Main Data Grid-->
			<Border Grid.Row="1">
				<controls:AdornedContentControl IsAdornerVisible="{Binding IsBusy, Mode=OneWay}">
					<controls:AdornedContentControl.AdornerContent>
						<controls:LoadingControl />
					</controls:AdornedContentControl.AdornerContent>

					<ListView AllowDrop="True" SelectionMode="Single" Drop="ListView_Drop"
							  ItemsSource="{Binding FilteredLogs}" SelectedItem="{Binding SelectedLog}">

						<i:Interaction.Behaviors>
							<behaviors:ScrollIntoViewBehavior />
						</i:Interaction.Behaviors>
						
						<ListView.ItemContainerStyle>
							<Style TargetType="{x:Type ListViewItem}">
								<Setter Property="Height" Value="20" />
								<Setter Property="VerticalContentAlignment" Value="Top" />

								<Style.Triggers>
									<DataTrigger Binding="{Binding Level}" Value="Fatal">
										<Setter Property="Background" Value="{Binding Source={StaticResource Locator}, Path=MainModel.SettingsModel.Fatal, Converter={StaticResource ColorToSolidColorBrushConverter}}"/>
									</DataTrigger>

									<DataTrigger Binding="{Binding Level}" Value="Error">
										<Setter Property="Background" Value="{Binding Source={StaticResource Locator}, Path=MainModel.SettingsModel.Error, Converter={StaticResource ColorToSolidColorBrushConverter}}"/>
									</DataTrigger>

									<DataTrigger Binding="{Binding Level}" Value="Warn">
										<Setter Property="Background" Value="{Binding Source={StaticResource Locator}, Path=MainModel.SettingsModel.Warn, Converter={StaticResource ColorToSolidColorBrushConverter}}"/>
									</DataTrigger>

									<DataTrigger Binding="{Binding Level}" Value="Info">
										<Setter Property="Background" Value="{Binding Source={StaticResource Locator}, Path=MainModel.SettingsModel.Info, Converter={StaticResource ColorToSolidColorBrushConverter}}"/>
									</DataTrigger>

									<DataTrigger Binding="{Binding Level}" Value="Debug">
										<Setter Property="Background" Value="{Binding Source={StaticResource Locator}, Path=MainModel.SettingsModel.Debug, Converter={StaticResource ColorToSolidColorBrushConverter}}"/>
									</DataTrigger>

									<DataTrigger Binding="{Binding Level}" Value="Unknown">
										<Setter Property="Background" Value="{Binding Source={StaticResource Locator}, Path=MainModel.SettingsModel.Unknown, Converter={StaticResource ColorToSolidColorBrushConverter}}"/>
									</DataTrigger>
								</Style.Triggers>
							</Style>
						</ListView.ItemContainerStyle>

						<ListView.View>
							<GridView>
								<GridViewColumn Header="Item" DisplayMemberBinding="{Binding ID}" />
								<GridViewColumn Header="Time Stamp" DisplayMemberBinding="{Binding TimeStamp, StringFormat='{}{0:MM/dd/yyyy HH:mm:ss.fff}'}" />
								<GridViewColumn Header="Level" DisplayMemberBinding="{Binding Level}" />
								<GridViewColumn Header="Message" DisplayMemberBinding="{Binding Message}" />
								<GridViewColumn Header="Thread" DisplayMemberBinding="{Binding Thread}" />
								<GridViewColumn Header="Machine" DisplayMemberBinding="{Binding Machine}" />
								<GridViewColumn Header="Host" DisplayMemberBinding="{Binding Host}" />
								<GridViewColumn Header="User" DisplayMemberBinding="{Binding User}" />
								<GridViewColumn Header="Application" DisplayMemberBinding="{Binding Application}" />
								<GridViewColumn Header="Class" DisplayMemberBinding="{Binding Class}" />
								<GridViewColumn Header="Method" DisplayMemberBinding="{Binding Method}" />
								<GridViewColumn Header="Line" DisplayMemberBinding="{Binding Line}" />
								<GridViewColumn Header="Logger" DisplayMemberBinding="{Binding Logger}" />
							</GridView>
						</ListView.View>
					</ListView>
				</controls:AdornedContentControl>
			</Border>

			<!-- Row 3 - Splitter -->
			<GridSplitter Grid.Row="2" ResizeDirection="Rows" Height="8" HorizontalAlignment="Stretch">
				<GridSplitter.Background>
					<RadialGradientBrush>
						<GradientStop Color="#FF4E4E4E" Offset="1" />
						<GradientStop Color="#FFB2B2B2" />
					</RadialGradientBrush>
				</GridSplitter.Background>
			</GridSplitter>

			<!-- Row 4 - Detailed Entry View -->
			<Border Grid.Row="3" Margin="0,5,0,0">
				<controls:DetailControl DataContext="{Binding SelectedLog}" />
			</Border>
		</Grid>
	</DockPanel>
</Window>
