﻿using System.Windows;

namespace Honu.LogViewer.App.Views
{
    /// <summary>
    /// Interaction logic for <see cref="SettingsView"/>.
    /// </summary>
    public partial class SettingsView : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsView"/> class.
        /// </summary>
        public SettingsView()
        { this.InitializeComponent(); }
    }
}
