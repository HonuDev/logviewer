﻿using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using Honu.LogViewer.App.Messages;

namespace Honu.LogViewer.App.Views
{
    /// <summary>
    /// Interaction logic for <see cref="MainView"/>.
    /// </summary>
    public partial class MainView : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainView"/> class.
        /// </summary>
        public MainView()
        { this.InitializeComponent(); }

        /// <summary>
        /// Handles the <see cref="Drop"/> event of the <see cref="ListView"/> control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DragEventArgs"/> instance containing the event data.</param>
        private void ListView_Drop(object sender, DragEventArgs e)
        {
            // Check if file drop
            string[] formats = e.Data.GetFormats();
            if (formats.Contains(DataFormats.FileDrop))
            {
                string[] files = e.Data.GetData(DataFormats.FileDrop) as string[];
                if (files != null && files.Length > 0)
                { Messenger.Default.Send<OpenFilesMessage>(new OpenFilesMessage(files)); }
            }
            else
            {
                MemoryStream stream = e.Data.GetData("FileContents", true) as MemoryStream;
                if (stream != null)
                {
                    Messenger.Default.Send<OpenStreamMessage>(new OpenStreamMessage(stream));
                }
            }
        }
    }
}
