﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Honu.LogViewer.App.ViewModels;

namespace Honu.LogViewer.App.Views
{
    /// <summary>
    /// Interaction logic for GoToLogView.xaml
    /// </summary>
    public partial class GoToLogView : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GoToLogView"/> class.
        /// </summary>
        /// <param name="ids">The collection of IDs.</param>
        /// <exception cref="ArgumentException">ID collection does not contain data.;ids</exception>
        public GoToLogView(IEnumerable<int> ids)
        {
            if (ids == null)
            { throw new ArgumentNullException("ids"); }
            if (ids.Count() <= 0)
            { throw new ArgumentException("ID collection does not contain data.", "ids"); }

            this.InitializeComponent();

            GoToLogViewModel model = this.Model;
            if (model != null)
            {
                model.Reset();
                model.AvailableIds = ids;
            }
        }

        /// <summary>
        /// Gets the model.
        /// </summary>
        private GoToLogViewModel Model
        { get { return this.DataContext as GoToLogViewModel; } }
    }
}
