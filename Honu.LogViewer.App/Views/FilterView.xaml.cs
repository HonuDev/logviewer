﻿using System.Windows;

namespace Honu.LogViewer.App.Views
{
    /// <summary>
    /// Interaction logic for <see cref="FilterView"/>.
    /// </summary>
    public partial class FilterView : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FilterView"/> class.
        /// </summary>
        public FilterView()
        { this.InitializeComponent(); }
    }
}
