﻿using System;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Honu.LogViewer.App.Messages;
using Honu.LogViewer.App.Models;

namespace Honu.LogViewer.App.ViewModels
{
    /// <summary>
    /// Settings View Model
    /// </summary>
    /// <seealso cref="GalaSoft.MvvmLight.ViewModelBase" />
    internal class SettingsViewModel : ViewModelBase
    {
        /// <summary>
        /// Settings model
        /// </summary>
        private readonly SettingsModel settings;

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsViewModel"/> class.
        /// </summary>
        /// <param name="settings">The settings.</param>
        public SettingsViewModel(SettingsModel settings)
        {
            if (settings == null)
            { throw new ArgumentNullException("settings"); }

            this.settings = settings;
            this.ApplyCommand = new RelayCommand(this.DoApply, this.CanDoApply);
            this.CancelCommand = new RelayCommand<Window>(this.DoCancel);
            this.SaveCommand = new RelayCommand<Window>(this.DoSave);
        }

        /// <summary>
        /// Gets the settings.
        /// </summary>
        public SettingsModel Settings
        { get { return this.settings; } }

        /// <summary>
        /// Gets the save command.
        /// </summary>
        public ICommand SaveCommand
        { get; private set; }

        /// <summary>
        /// Gets the cancel command.
        /// </summary>
        public ICommand CancelCommand
        { get; private set; }

        /// <summary>
        /// Gets the apply command.
        /// </summary>
        public ICommand ApplyCommand
        { get; private set; }

        /// <summary>
        /// Determines whether this instance can perform the Apply command.
        /// </summary>
        /// <returns><c>true</c> if this instance can perform the Apply command.</returns>
        public bool CanDoApply()
        { return this.settings.IsModified; }

        /// <summary>
        /// Does the apply.
        /// </summary>
        public void DoApply()
        { this.settings.CommitChanges(); }

        /// <summary>
        /// Does the save.
        /// </summary>
        /// <param name="window">The window to be closed when the command is complete.</param>
        public void DoSave(Window window)
        { 
            if (this.settings.IsModified)
            { this.settings.CommitChanges(); }

            this.MessengerInstance.Send<CloseWindowMessage>(new CloseWindowMessage(window));
        }

        /// <summary>
        /// Does the cancel.
        /// </summary>
        /// <param name="window">The window to be closed when the command is complete.</param>
        public void DoCancel(Window window)
        {
            if (this.settings.IsModified)
            { this.settings.Reset(); }

            this.MessengerInstance.Send<CloseWindowMessage>(new CloseWindowMessage(window));
        }
    }
}
