﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using System.Text;
using System.Windows;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Honu.LogViewer.App.Messages;
using Honu.LogViewer.App.Properties;
using log4net;

namespace Honu.LogViewer.App.ViewModels
{
    /// <summary>
    /// About window view model
    /// </summary>
    internal class AboutViewModel : ViewModelBase
    {
        /// <summary>
        /// <see cref="AboutViewModel"/> logger
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(AboutViewModel));

        /// <summary>
        /// Assembly name
        /// </summary>
        private string name;

        /// <summary>
        /// Assembly information
        /// </summary>
        private string info;

        /// <summary>
        /// Initializes a new instance of the <see cref="AboutViewModel" /> class.
        /// </summary>
        public AboutViewModel()
        {
            Assembly assm = Assembly.GetExecutingAssembly();

            // Build title
            AssemblyTitleAttribute title = assm.GetCustomAttribute<AssemblyTitleAttribute>();
            this.AssemblyName = string.Format("{0} {1}", title.Title, assm.GetName().Version.ToString(2)).Trim();

            // Build assembly information
            StringBuilder sb = new StringBuilder();
            AssemblyCopyrightAttribute attrCopy = assm.GetCustomAttribute<AssemblyCopyrightAttribute>();
            if (attrCopy != null)
            { sb.AppendLine(attrCopy.Copyright); }

            AssemblyCompanyAttribute attrCompany = assm.GetCustomAttribute<AssemblyCompanyAttribute>();
            if (attrCompany != null)
            { sb.AppendLine(attrCompany.Company); }

            AssemblyDescriptionAttribute attrDesc = assm.GetCustomAttribute<AssemblyDescriptionAttribute>();
            if (attrDesc != null)
            { sb.AppendLine(attrDesc.Description); }

            this.AssemblyInfo = sb.ToString();

            // Setup commands
            this.CloseCommand = new RelayCommand<Window>(this.Close);
            this.NavigateCommand = new RelayCommand<string>(this.Navigate);
        }

        /// <summary>
        /// Gets the assembly name
        /// </summary>
        public string AssemblyName
        {
            get { return this.name; }
            private set { this.Set<string>(() => this.AssemblyName, ref this.name, value); }
        }

        /// <summary>
        /// Gets the assembly information
        /// </summary>
        public string AssemblyInfo
        {
            get { return this.info; }
            private set { this.Set<string>(() => this.AssemblyInfo, ref this.info, value); }
        }

        /// <summary>
        /// Gets the example configuration
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public string ExampleConfiguration
        { get { return Resources.ExampleConfig; } }

        /// <summary>
        /// Gets or sets the close command
        /// </summary>
        public RelayCommand<Window> CloseCommand
        { get; set; }

        /// <summary>
        /// Gets or sets the navigate command
        /// </summary>
        public RelayCommand<string> NavigateCommand
        { get; set; }

        /// <summary>
        /// Closes the window
        /// </summary>
        /// <param name="window">The window to be closed</param>
        private void Close(Window window)
        { this.MessengerInstance.Send<CloseWindowMessage>(new CloseWindowMessage(window)); }

        /// <summary>
        /// Navigates to the specified path
        /// </summary>
        /// <param name="path">The location to navigate</param>
        private void Navigate(string path)
        {
            try
            {
                Log.InfoFormat("Navigating to hyperlink: {0}", path);
                Process.Start(path);
            }
            catch (Exception ex)
            { Log.Error(ex.Message, ex); }
        }
    }
}
