﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Honu.LogViewer.App.Messages;

namespace Honu.LogViewer.App.ViewModels
{
    /// <summary>
    /// Go To Log View Model
    /// </summary>
    /// <seealso cref="GalaSoft.MvvmLight.ViewModelBase" />
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:ElementsMustBeDocumented", Justification = "Reviewed.")]
    internal sealed class GoToLogViewModel : ViewModelBase
    {
        private string requestedLog;
        private IEnumerable<int> availableIds;

        /// <summary>
        /// Initializes a new instance of the <see cref="GoToLogViewModel"/> class.
        /// </summary>
        public GoToLogViewModel()
        {
            this.OkCommand = new RelayCommand<Window>(this.DoOk, this.CanDoOk);
            this.CancelCommand = new RelayCommand<Window>(this.DoCancel);
        }

        /// <summary>
        /// Gets the OK command.
        /// </summary>
        public ICommand OkCommand
        { get; private set; }

        /// <summary>
        /// Gets the cancel command.
        /// </summary>
        public ICommand CancelCommand
        { get; private set; }

        /// <summary>
        /// Gets or sets the available IDs.
        /// </summary>
        public IEnumerable<int> AvailableIds
        {
            get { return this.availableIds; }
            set { this.Set(() => this.AvailableIds, ref this.availableIds, value); }
        }

        /// <summary>
        /// Gets or sets the requested log.
        /// </summary>
        public string RequestedLog
        {
            get { return this.requestedLog; }
            set
            {
                if (this.Set(() => this.RequestedLog, ref this.requestedLog, value))
                { this.RaisePropertyChanged(() => this.RequestedLogId); }
            }
        }

        /// <summary>
        /// Gets the requested log identifier.
        /// </summary>
        public int RequestedLogId
        {
            get
            {
                int id = 0;
                return int.TryParse(this.RequestedLog, out id) ? id : 0;
            }
        }

        /// <summary>
        /// Resets this instance.
        /// </summary>
        public void Reset()
        {
            this.AvailableIds = null;
            this.RequestedLog = string.Empty;
        }

        /// <summary>
        /// Does the Cancel command.
        /// </summary>
        /// <param name="window">The window to be closed.</param>
        private void DoCancel(Window window)
        { this.MessengerInstance.Send<CloseWindowMessage>(new CloseWindowMessage(window)); }

        /// <summary>
        /// Does the OK command.
        /// </summary>
        /// <param name="window">The window to be closed.</param>
        private void DoOk(Window window)
        {
            // Retrieve value and reset model before sending message
            int value = this.RequestedLogId;
            this.Reset();

            this.MessengerInstance.Send<GoToLogMessage>(new GoToLogMessage(value));
            this.MessengerInstance.Send<CloseWindowMessage>(new CloseWindowMessage(window));
        }

        /// <summary>
        /// Determines whether the model can perform the OK command.
        /// </summary>
        /// <param name="window">The window to be closed.</param>
        /// <returns><c>true</c> if the model can perform the OK command.</returns>
        private bool CanDoOk(Window window)
        {
            // Check for available IDs
            if (this.AvailableIds == null || this.AvailableIds.Count() == 0)
            { return false; }

            // Check for requested log
            return this.AvailableIds.Contains(this.RequestedLogId);
        }
    }
}
