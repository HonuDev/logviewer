﻿using System.Diagnostics.CodeAnalysis;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Honu.LogViewer.App.Models;
using Microsoft.Practices.ServiceLocation;

namespace Honu.LogViewer.App.ViewModels
{
    /// <summary>
    /// View Model Locator
    /// </summary>
    /// <seealso cref="GalaSoft.MvvmLight.ViewModelBase" />
    internal sealed class ViewModelLocator : ViewModelBase
    {
        /// <summary>
        /// Initializes static members of the <see cref="ViewModelLocator"/> class.
        /// </summary>
        static ViewModelLocator()
        {
            // Set locator provider
            ISimpleIoc ioc = SimpleIoc.Default;

            if (!ServiceLocator.IsLocationProviderSet)
            { ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default); }

            // Setup common models for application
            ioc.Register<SettingsModel>();
            ioc.Register<FilterModel>();

            // Setup view models
            ioc.Register<MainViewModel>();
            ioc.Register<SettingsViewModel>();
            ioc.Register<FilterViewModel>(true);
            ioc.Register<GoToLogViewModel>();
        }

        /// <summary>
        /// Gets the main model.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public MainViewModel MainModel
        { get { return ServiceLocator.Current.GetInstance<MainViewModel>(); } }

        /// <summary>
        /// Gets the settings model.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public SettingsViewModel SettingsModel
        { get { return ServiceLocator.Current.GetInstance<SettingsViewModel>(); } }

        /// <summary>
        /// Gets the filter model.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public FilterViewModel FilterModel
        { get { return ServiceLocator.Current.GetInstance<FilterViewModel>(); } }

        /// <summary>
        /// Gets the go to log model.
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Justification = "This non-static member is needed for data binding purposes.")]
        public GoToLogViewModel GoToLogModel
        { get { return ServiceLocator.Current.GetInstance<GoToLogViewModel>(); } }
    }
}
