﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Honu.LogViewer.App.Messages;
using Honu.LogViewer.App.Models;
using log4net;

namespace Honu.LogViewer.App.ViewModels
{
    /// <summary>
    /// Main View Model
    /// </summary>
    /// <seealso cref="GalaSoft.MvvmLight.ViewModelBase" />
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:ElementsMustBeDocumented", Justification = "Reviewed.")]
    internal sealed class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Default application title
        /// </summary>
        private const string DefaultTitle = "Log Viewer";

        /// <summary>
        /// <see cref="MainViewModel"/> logger
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(MainViewModel));

        private Guid currentFilteringTaskId;
        private string title;
        private bool isBusy;
        private ClientLogEntry selectedLog;
        private IEnumerable<string> openedFiles;
        private IEnumerable<ClientLogEntry> logs;
        private IEnumerable<ClientLogEntry> filteredLogs;
        private int debugCount;
        private int infoCount;
        private int warnCount;
        private int errorCount;
        private int fatalCount;
        private int unknownCount;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <param name="filter">The filter.</param>
        public MainViewModel(SettingsModel settings, FilterModel filter)
        {
            if (ViewModelBase.IsInDesignModeStatic)
            {
                this.Title = string.Concat(DefaultTitle, " [Design]");
                this.OpenCommand = ApplicationCommands.NotACommand;
            }
            else
            {
                if (settings == null)
                { throw new ArgumentNullException("settings"); }
                if (filter == null)
                { throw new ArgumentNullException("filter"); }

                this.Title = DefaultTitle;
                this.SettingsModel = settings;
                this.FilterModel = filter;
                this.FilterModel.FilterChanged += this.FilterModel_FilterChanged;
                this.FilterModel.FilterCleared += this.FilterModel_FilterCleared;

                this.LogModel = new LogModel();
                this.RecentFileModel = new RecentFileModel();
                this.Logs = new ObservableCollection<ClientLogEntry>();

                this.AboutCommand = new RelayCommand(this.DoAbout);
                this.SettingsCommand = new RelayCommand(this.DoSettings);
                this.OpenCommand = new RelayCommand(this.DoOpen, this.CanDoOpen);
                this.FilterCommand = new RelayCommand(this.DoFilter, this.CanDoFilter);
                this.RefreshCommand = new RelayCommand(this.DoRefresh, this.CanDoRefresh);
                this.ClearFilterCommand = new RelayCommand(this.DoClearFilter);
                this.RequestGoToLogCommand = new RelayCommand(this.DoRequestGoToLog);
                this.CloseWindowCommand = new RelayCommand<Window>(this.DoCloseWindow);
            }

            this.MessengerInstance.Register<OpenFilesMessage>(this, this.DoOpenFilesMessage);
            this.MessengerInstance.Register<OpenStreamMessage>(this, this.DoOpenStreamMessage);
            this.MessengerInstance.Register<GoToLogMessage>(this, this.DoGoToLogMessage);
        }

        /// <summary>
        /// Gets the Open command
        /// </summary>
        public ICommand OpenCommand
        { get; private set; }

        /// <summary>
        /// Gets the About command.
        /// </summary>
        public ICommand AboutCommand
        { get; private set; }

        /// <summary>
        /// Gets the Settings command.
        /// </summary>
        public ICommand SettingsCommand
        { get; private set; }

        /// <summary>
        /// Gets the Refresh command.
        /// </summary>
        public ICommand RefreshCommand
        { get; private set; }

        /// <summary>
        /// Gets the Filter command.
        /// </summary>
        public ICommand FilterCommand
        { get; private set; }

        /// <summary>
        /// Gets the Go To Log command.
        /// </summary>
        public ICommand RequestGoToLogCommand
        { get; private set; }

        /// <summary>
        /// Gets the Clear Filter command.
        /// </summary>
        public ICommand ClearFilterCommand
        { get; private set; }

        /// <summary>
        /// Gets the Close Window command.
        /// </summary>
        public ICommand CloseWindowCommand
        { get; private set; }

        /// <summary>
        /// Gets the settings model.
        /// </summary>
        public SettingsModel SettingsModel
        { get; private set; }

        /// <summary>
        /// Gets the log model.
        /// </summary>
        public LogModel LogModel
        { get; private set; }

        /// <summary>
        /// Gets the filter model.
        /// </summary>
        public FilterModel FilterModel
        { get; private set; }

        /// <summary>
        /// Gets the recent file model.
        /// </summary>
        public RecentFileModel RecentFileModel
        { get; private set; }

        /// <summary>
        /// Gets the logs.
        /// </summary>
        public IEnumerable<ClientLogEntry> Logs
        {
            get { return this.logs; }
            private set
            {
                if (this.Set(() => this.Logs, ref this.logs, value))
                { this.RaisePropertyChanged(() => this.FullLogCount); }
            }
        }

        /// <summary>
        /// Gets the filtered logs.
        /// </summary>
        public IEnumerable<ClientLogEntry> FilteredLogs
        {
            get { return this.filteredLogs; }
            private set
            {
                if (this.Set(() => this.FilteredLogs, ref this.filteredLogs, value))
                {
                    this.OnFilteredLogsChanged();
                    this.RaisePropertyChanged(() => this.DebugLogCount);
                    this.RaisePropertyChanged(() => this.InfoLogCount);
                    this.RaisePropertyChanged(() => this.WarnLogCount);
                    this.RaisePropertyChanged(() => this.ErrorLogCount);
                    this.RaisePropertyChanged(() => this.FatalLogCount);
                    this.RaisePropertyChanged(() => this.UnknownLogCount);
                }
            }
        }

        /// <summary>
        /// Gets the opened files.
        /// </summary>
        public IEnumerable<string> OpenedFiles
        {
            get { return this.openedFiles; }
            private set { this.Set(() => this.OpenedFiles, ref this.openedFiles, value); }
        }

        /// <summary>
        /// Gets or sets the selected log.
        /// </summary>
        public ClientLogEntry SelectedLog
        {
            get { return this.selectedLog; }
            set { this.Set(() => this.SelectedLog, ref this.selectedLog, value); }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is busy.
        /// </summary>
        public bool IsBusy
        {
            get { return this.isBusy; }
            private set { this.Set(() => this.IsBusy, ref this.isBusy, value); }
        }

        /// <summary>
        /// Gets the title.
        /// </summary>
        public string Title
        {
            get { return this.title; }
            private set { this.Set(() => this.Title, ref this.title, value); }
        }

        /// <summary>
        /// Gets the full log count.
        /// </summary>
        public int FullLogCount
        { get { return this.Logs == null ? 0 : this.Logs.Count(); } }

        /// <summary>
        /// Gets the debug log count.
        /// </summary>
        public int DebugLogCount
        {
            get { return this.debugCount; }
            private set { this.Set(() => this.DebugLogCount, ref this.debugCount, value); }
        }

        /// <summary>
        /// Gets the information log count.
        /// </summary>
        public int InfoLogCount
        {
            get { return this.infoCount; }
            private set { this.Set(() => this.InfoLogCount, ref this.infoCount, value); }
        }

        /// <summary>
        /// Gets the warn log count.
        /// </summary>
        public int WarnLogCount
        {
            get { return this.warnCount; }
            private set { this.Set(() => this.WarnLogCount, ref this.warnCount, value); }
        }

        /// <summary>
        /// Gets the error log count.
        /// </summary>
        public int ErrorLogCount
        {
            get { return this.errorCount; }
            private set { this.Set(() => this.ErrorLogCount, ref this.errorCount, value); }
        }

        /// <summary>
        /// Gets the fatal log count.
        /// </summary>
        public int FatalLogCount
        {
            get { return this.fatalCount; }
            private set { this.Set(() => this.FatalLogCount, ref this.fatalCount, value); }
        }

        /// <summary>
        /// Gets the unknown log count.
        /// </summary>
        public int UnknownLogCount
        {
            get { return this.unknownCount; }
            private set { this.Set(() => this.UnknownLogCount, ref this.unknownCount, value); }
        }

        /// <summary>
        /// Gets the filtered log count.
        /// </summary>
        public int FilteredLogCount
        { get { return this.DebugLogCount + this.InfoLogCount + this.WarnLogCount + this.ErrorLogCount + this.FatalLogCount + this.UnknownLogCount; } }

        /// <summary>
        /// Gets a value indicating whether this instance is displaying filtered view.
        /// </summary>
        public bool IsDisplayingFilteredView
        { get { return this.FilteredLogCount != this.FullLogCount; } }

        /// <summary>
        /// Does the about command.
        /// </summary>
        private void DoAbout()
        { this.MessengerInstance.Send<ShowAboutMessage>(new ShowAboutMessage()); }

        /// <summary>
        /// Does the settings command.
        /// </summary>
        private void DoSettings()
        { this.MessengerInstance.Send<ShowSettingsMessage>(new ShowSettingsMessage()); }

        /// <summary>
        /// Does the Open command
        /// </summary>
        private void DoOpen()
        { this.MessengerInstance.Send<RequestOpenFileDialogMessage>(new RequestOpenFileDialogMessage()); }

        /// <summary>
        /// Does the Filter command.
        /// </summary>
        private void DoFilter()
        { this.MessengerInstance.Send<ShowFilterMessage>(new ShowFilterMessage()); }

        /// <summary>
        /// Does the close window command.
        /// </summary>
        /// <param name="window">The window to be closed.</param>
        private void DoCloseWindow(Window window)
        { this.MessengerInstance.Send<CloseWindowMessage>(new CloseWindowMessage(window)); }

        /// <summary>
        /// Does the Request Go To Log command.
        /// </summary>
        private void DoRequestGoToLog()
        { this.MessengerInstance.Send<RequestGoToLogDialogMessage>(new RequestGoToLogDialogMessage(this.FilteredLogs.Select(l => l.ID))); }

        /// <summary>
        /// Does the refresh command.
        /// </summary>
        private void DoRefresh()
        {
            // Route command to open files message receipt
            if (this.OpenedFiles != null && this.OpenedFiles.Count() > 0)
            { this.DoOpenFilesMessage(new OpenFilesMessage(this.OpenedFiles)); }
        }

        /// <summary>
        /// Indicates whether the model can execute the Open command.
        /// </summary>
        /// <returns><c>true</c> if the Open command may be executed.</returns>
        private bool CanDoOpen()
        { return !this.IsBusy; }

        /// <summary>
        /// Indicates whether the model can execute the Filter command.
        /// </summary>
        /// <returns><c>true</c> if the Filter command may be executed.</returns>
        private bool CanDoFilter()
        { return this.Logs != null && this.Logs.Count() > 0; }

        /// <summary>
        /// Indicates whether the model can execute the Refresh command.
        /// </summary>
        /// <returns><c>true</c> if the Refresh command may be executed.</returns>
        private bool CanDoRefresh()
        { return this.OpenedFiles != null && this.OpenedFiles.Count() > 0; }

        /// <summary>
        /// Clears the filter.
        /// </summary>
        private void DoClearFilter()
        { this.FilterModel.Clear(); }

        /// <summary>
        /// Does the open stream message.
        /// </summary>
        /// <param name="message">The message.</param>
        private async void DoOpenStreamMessage(OpenStreamMessage message)
        {
            if (this.IsBusy)
            { return; }

            try
            {
                this.IsBusy = true;

                IEnumerable<ClientLogEntry> results = await this.LogModel.RetrieveLogsAsync(message.Content);
                await this.SetNewLogs(results);
            }
            catch (Exception ex)
            {
                // Update the VM with error information.
                Log.Error(ex.Message, ex);
                this.Title = DefaultTitle;
            }
            finally
            { this.IsBusy = false; }
        }

        /// <summary>
        /// Does the open files message.
        /// </summary>
        /// <param name="message">The message.</param>
        private async void DoOpenFilesMessage(OpenFilesMessage message)
        {
            if (this.IsBusy)
            { return; }

            try
            {
                this.IsBusy = true;

                // Load logs
                IEnumerable<ClientLogEntry> results = await this.LogModel.RetrieveLogsAsync(message.Content);
                await this.SetNewLogs(results);
            }
            catch (Exception ex)
            {
                // Update the VM with error information.
                Log.Error(ex.Message, ex);
                this.Title = DefaultTitle;
            }
            finally
            { this.IsBusy = false; }
        }

        /// <summary>
        /// Does the go to log message.
        /// </summary>
        /// <param name="msg">The message.</param>
        private void DoGoToLogMessage(GoToLogMessage msg)
        {
            if (msg == null)
            { return; }

            // Set log with given ID to selected item
            this.SelectedLog = this.FilteredLogs.FirstOrDefault(l => l.ID == msg.Content);
        }

        /// <summary>
        /// Sets the new logs.
        /// </summary>
        /// <param name="logs">The logs.</param>
        /// <returns>Task handle</returns>
        private async Task SetNewLogs(IEnumerable<ClientLogEntry> logs)
        {
            // Send logs retrieved message
            int logCount = logs == null ? 0 : logs.Count();
            this.Logs = logCount > 0 ? logs.OrderBy(e => e.ID) : null;
            this.MessengerInstance.Send<LogsRetrievedMessage>(new LogsRetrievedMessage(this.Logs));

            // Update filtered logs
            this.FilteredLogs = this.FilterModel.IsEmpty ? this.Logs : await this.CalculateFilteredEntriesAsync();

            // Load title
            this.OpenedFiles = logs.Select(l => l.LogFile).Where(l => !string.IsNullOrWhiteSpace(l)).Distinct();
            int fileCount = this.OpenedFiles == null ? 0 : this.OpenedFiles.Count();
            this.Title = fileCount == 0 ? DefaultTitle : fileCount == 1 ? string.Format("{0} - {1}", DefaultTitle, this.OpenedFiles.First()) : string.Format("{0} - Multiple", DefaultTitle);

            // Because why not!
            this.Title = logCount == 0
                ? DefaultTitle
                : fileCount <= 0
                    ? string.Format("{0} - Attachment", DefaultTitle)
                    : fileCount == 1
                        ? string.Format("{0} - {1}", DefaultTitle, this.OpenedFiles.First())
                        : string.Format("{0} - Multiple", DefaultTitle);

            // Update recent file list
            if (fileCount > 0)
            { this.RecentFileModel.Add(this.OpenedFiles); }
        }

        /// <summary>
        /// Handles the <see cref="FilterCleared"/> event of the <see cref="FilterModel"/> control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void FilterModel_FilterCleared(object sender, EventArgs e)
        {
            // Set filtered logs to complete set of logs
            this.FilteredLogs = this.Logs;
        }

        /// <summary>
        /// Handles the <see cref="FilterChanged"/> event of the <see cref="FilterModel"/> control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private async void FilterModel_FilterChanged(object sender, EventArgs e)
        {
            try
            {
                Guid currentTask = Guid.NewGuid();
                this.currentFilteringTaskId = currentTask;

                // Await filtered entries
                List<ClientLogEntry> filteredEntries = await this.CalculateFilteredEntriesAsync();

                // If we are still the latest instance - set to filtered task
                if (Guid.Equals(this.currentFilteringTaskId, currentTask))
                { this.FilteredLogs = filteredEntries; }
            }
            catch (Exception ex)
            { Log.Error(ex.Message, ex); }
        }

        /// <summary>
        /// Calculates the filtered entries asynchronously.
        /// </summary>
        /// <returns>Task handle which performs the calculation.</returns>
        private Task<List<ClientLogEntry>> CalculateFilteredEntriesAsync()
        {
            if (this.Logs == null)
            { return Task.FromResult<List<ClientLogEntry>>(null); }

            // Build query
            IQueryable<ClientLogEntry> entries = this.Logs.AsQueryable();
            FilterModel filter = this.FilterModel;
            if (filter != null && !filter.IsEmpty)
            {
                if (!string.IsNullOrWhiteSpace(filter.Application))
                { entries = entries.Where(e => string.Equals(e.Application, filter.Application)); }

                if (!string.IsNullOrWhiteSpace(filter.Class))
                { entries = entries.Where(e => string.Equals(e.Class, filter.Class)); }

                if (!string.IsNullOrWhiteSpace(filter.CodeFile))
                { entries = entries.Where(e => string.Equals(e.CodeFile, filter.CodeFile)); }

                if (!string.IsNullOrWhiteSpace(filter.Logger))
                { entries = entries.Where(e => string.Equals(e.Logger, filter.Logger)); }

                if (!string.IsNullOrWhiteSpace(filter.Machine))
                { entries = entries.Where(e => string.Equals(e.Machine, filter.Machine)); }

                if (!string.IsNullOrWhiteSpace(filter.Message))
                { entries = entries.Where(e => e.Message.IndexOf(filter.Message, StringComparison.OrdinalIgnoreCase) > -1); }

                if (!string.IsNullOrWhiteSpace(filter.Method))
                { entries = entries.Where(e => string.Equals(e.Method, filter.Method)); }

                if (!string.IsNullOrWhiteSpace(filter.Thread))
                { entries = entries.Where(e => string.Equals(e.Thread, filter.Thread)); }

                if (!string.IsNullOrWhiteSpace(filter.User))
                { entries = entries.Where(e => string.Equals(e.User, filter.User)); }

                List<LogLevel> levels = new List<LogLevel>();
                if (filter.DebugLevel)
                { levels.Add(LogLevel.Debug); }

                if (filter.InfoLevel)
                { levels.Add(LogLevel.Info); }

                if (filter.WarnLevel)
                { levels.Add(LogLevel.Warn); }

                if (filter.ErrorLevel)
                { levels.Add(LogLevel.Error); }

                if (filter.FatalLevel)
                { levels.Add(LogLevel.Fatal); }

                if (filter.UnknownLevel)
                { levels.Add(LogLevel.Unknown); }

                if (levels.Count > 0)
                { entries = entries.Where(e => levels.Contains(e.Level)); }
            }

            return Task.Run<List<ClientLogEntry>>(() => { return entries.ToList(); });
        }

        /// <summary>
        /// Called when filtered logs have changed.
        /// </summary>
        private void OnFilteredLogsChanged()
        {
            if (this.FilteredLogs != null)
            {
                ILookup<LogLevel, ClientLogEntry> levelCounts = this.FilteredLogs.ToLookup(k => k.Level);

                this.DebugLogCount = levelCounts[LogLevel.Debug].Count();
                this.InfoLogCount = levelCounts[LogLevel.Info].Count();
                this.WarnLogCount = levelCounts[LogLevel.Warn].Count();
                this.ErrorLogCount = levelCounts[LogLevel.Error].Count();
                this.FatalLogCount = levelCounts[LogLevel.Fatal].Count();
                this.UnknownLogCount = levelCounts[LogLevel.Unknown].Count();
            }
            else
            {
                this.DebugLogCount = 0;
                this.InfoLogCount = 0;
                this.WarnLogCount = 0;
                this.ErrorLogCount = 0;
                this.FatalLogCount = 0;
                this.UnknownLogCount = 0;
            }

            this.RaisePropertyChanged(() => this.FilteredLogCount);
            this.RaisePropertyChanged(() => this.IsDisplayingFilteredView);
        }
    }
}