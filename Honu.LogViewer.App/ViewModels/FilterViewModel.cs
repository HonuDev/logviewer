﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Honu.LogViewer.App.Messages;
using Honu.LogViewer.App.Models;

namespace Honu.LogViewer.App.ViewModels
{
    /// <summary>
    /// Filter View Model
    /// </summary>
    /// <seealso cref="GalaSoft.MvvmLight.ViewModelBase" />
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:ElementsMustBeDocumented", Justification = "Reviewed.")]
    internal sealed class FilterViewModel : ViewModelBase
    {
        /// <summary>
        /// Filter model
        /// </summary>
        private readonly FilterModel filter;

        private readonly ObservableCollection<string> userList;
        private readonly ObservableCollection<string> threadList;
        private readonly ObservableCollection<string> machineList;
        private readonly ObservableCollection<string> applicationList;
        private readonly ObservableCollection<string> classList;
        private readonly ObservableCollection<string> methodList;
        private readonly ObservableCollection<string> codeFileList;
        private readonly ObservableCollection<string> loggerList;
        private readonly ObservableCollection<string> hostList;

        /// <summary>
        /// Initializes a new instance of the <see cref="FilterViewModel"/> class.
        /// </summary>
        /// <param name="filter">The filter model.</param>
        public FilterViewModel(FilterModel filter)
        {
            if (filter == null)
            { throw new ArgumentNullException("filter"); }

            this.filter = filter;

            this.applicationList = new ObservableCollection<string>();
            this.classList = new ObservableCollection<string>();
            this.codeFileList = new ObservableCollection<string>();
            this.hostList = new ObservableCollection<string>();
            this.loggerList = new ObservableCollection<string>();
            this.machineList = new ObservableCollection<string>();
            this.methodList = new ObservableCollection<string>();
            this.threadList = new ObservableCollection<string>();
            this.userList = new ObservableCollection<string>();

            this.ClearCommand = new RelayCommand(this.DoClear, this.CanDoClear);
            this.CloseCommand = new RelayCommand<Window>(this.DoClose);

            this.MessengerInstance.Register<LogsRetrievedMessage>(this, this.DoLogsRetrievedMessage);
        }

        /// <summary>
        /// Gets the Close command.
        /// </summary>
        public ICommand CloseCommand
        { get; private set; }

        /// <summary>
        /// Gets the Clear command.
        /// </summary>
        public ICommand ClearCommand
        { get; private set; }

        /// <summary>
        /// Gets the filter.
        /// </summary>
        public FilterModel Filter
        { get { return this.filter; } }

        /// <summary>
        /// Gets the applications.
        /// </summary>
        public ObservableCollection<string> Applications
        { get { return this.applicationList; } }

        /// <summary>
        /// Gets the classes.
        /// </summary>
        public ObservableCollection<string> Classes
        { get { return this.classList; } }

        /// <summary>
        /// Gets the code files.
        /// </summary>
        public ObservableCollection<string> CodeFiles
        { get { return this.codeFileList; } }

        /// <summary>
        /// Gets the hosts.
        /// </summary>
        public ObservableCollection<string> Hosts
        { get { return this.hostList; } }

        /// <summary>
        /// Gets the loggers.
        /// </summary>
        public ObservableCollection<string> Loggers
        { get { return this.loggerList; } }

        /// <summary>
        /// Gets the machines.
        /// </summary>
        public ObservableCollection<string> Machines
        { get { return this.machineList; } }

        /// <summary>
        /// Gets the methods.
        /// </summary>
        public ObservableCollection<string> Methods
        { get { return this.methodList; } }

        /// <summary>
        /// Gets the threads.
        /// </summary>
        public ObservableCollection<string> Threads
        { get { return this.threadList; } }

        /// <summary>
        /// Gets the users.
        /// </summary>
        public ObservableCollection<string> Users
        { get { return this.userList; } }

        /// <summary>
        /// Clears the lists.
        /// </summary>
        private void ClearLists()
        {
            this.Applications.Clear();
            this.Classes.Clear();
            this.CodeFiles.Clear();
            this.Loggers.Clear();
            this.Machines.Clear();
            this.Methods.Clear();
            this.Threads.Clear();
            this.Users.Clear();
        }

        /// <summary>
        /// Indicates if this instance can execute the Clear command.
        /// </summary>
        /// <returns><c>true</c> if the Clear command can be executed.</returns>
        private bool CanDoClear()
        { return !this.filter.IsEmpty; }

        /// <summary>
        /// Does the Clear command.
        /// </summary>
        private void DoClear()
        { this.filter.Clear(); }

        /// <summary>
        /// Does the Close command.
        /// </summary>
        /// <param name="window">The window to be closed.</param>
        private void DoClose(Window window)
        { this.MessengerInstance.Send<CloseWindowMessage>(new CloseWindowMessage(window)); }

        /// <summary>
        /// Reloads the collections with the information contained in the message.
        /// </summary>
        /// <param name="msg">The message containing log entries.</param>
        private void DoLogsRetrievedMessage(LogsRetrievedMessage msg)
        {
            if (msg == null)
            { throw new ArgumentNullException("msg"); }

            this.ClearLists();

            if (msg.Content != null)
            {
                Task.Run(() =>
                    {
                        IEnumerable<ClientLogEntry> entries = msg.Content;

                        foreach (string app in entries.Select(e => e.Application).Where(a => !string.IsNullOrWhiteSpace(a)).OrderBy(a => a).Distinct())
                        { this.Applications.Add(app); }

                        foreach (string @class in entries.Select(e => e.Class).Where(c => !string.IsNullOrWhiteSpace(c)).OrderBy(c => c).Distinct())
                        { this.Classes.Add(@class); }

                        foreach (string codeFile in entries.Select(e => e.CodeFile).Where(c => !string.IsNullOrWhiteSpace(c)).OrderBy(c => c).Distinct())
                        { this.CodeFiles.Add(codeFile); }

                        foreach (string host in entries.Select(e => e.Host).Where(h => !string.IsNullOrWhiteSpace(h)).OrderBy(h => h).Distinct())
                        { this.Hosts.Add(host); }

                        foreach (string logger in entries.Select(e => e.Logger).Where(l => !string.IsNullOrWhiteSpace(l)).OrderBy(l => l).Distinct())
                        { this.Loggers.Add(logger); }

                        foreach (string machine in entries.Select(e => e.Machine).Where(m => !string.IsNullOrWhiteSpace(m)).OrderBy(m => m).Distinct())
                        { this.Machines.Add(machine); }

                        foreach (string method in entries.Select(e => e.Method).Where(m => !string.IsNullOrWhiteSpace(m)).OrderBy(m => m).Distinct())
                        { this.Methods.Add(method); }

                        foreach (string thread in entries.Select(e => e.Thread).Where(t => !string.IsNullOrWhiteSpace(t)).OrderBy(t => t).Distinct())
                        { this.Threads.Add(thread); }

                        foreach (string user in entries.Select(e => e.User).Where(u => !string.IsNullOrWhiteSpace(u)).OrderBy(u => u).Distinct())
                        { this.Users.Add(user); }
                    });
            }
        }
    }
}