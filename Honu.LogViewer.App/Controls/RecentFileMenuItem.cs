﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using GalaSoft.MvvmLight.Messaging;
using Honu.LogViewer.App.Messages;
using log4net;

namespace Honu.LogViewer.App.Controls
{
    /// <summary>
    /// Recent File Menu Item
    /// </summary>
    internal class RecentFileMenuItem : MenuItem
    {
        /// <summary>
        /// Recent files dependency property
        /// </summary>
        public static readonly DependencyProperty RecentFilesProperty = DependencyProperty.Register(
            "RecentFiles",
            typeof(IEnumerable<string>),
            typeof(RecentFileMenuItem),
            new FrameworkPropertyMetadata(RecentFilesPropertyChanged));

        /// <summary>
        /// <see cref="RecentFileList"/> logger
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(RecentFileMenuItem));

        /// <summary>
        /// File List
        /// </summary>
        private ObservableCollection<string> fileList;

        /// <summary>
        /// Gets or sets the recent files list
        /// </summary>
        [Category("Behavior")]
        [Description("Gets or sets the list used to populate the recent files")]
        public IEnumerable<string> RecentFiles
        {
            get { return this.GetValue(RecentFilesProperty) as IEnumerable<string>; }
            set { this.SetValue(RecentFilesProperty, value); }
        }

        /// <summary>
        /// Occurs when a recent file menu item had been clicked
        /// </summary>
        /// <param name="path">The path of the recent file clicked</param>
        protected virtual void OnRecentFileClick(string path)
        {
            Log.DebugFormat("Sending OpenFilesMessage for path '{0}'", path);
            Messenger.Default.Send(new OpenFilesMessage(new List<string> { path }));
        }

        /// <summary>
        /// Occurs when the recent files property has changed
        /// </summary>
        /// <param name="d">Dependency object</param>
        /// <param name="e">Event arguments</param>
        private static void RecentFilesPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            // Cause items to reload
            RecentFileMenuItem list = d as RecentFileMenuItem;
            list.SetFileCollection(e.NewValue as ObservableCollection<string>);
        }

        /// <summary>
        /// Sets the initial file collection
        /// </summary>
        /// <param name="list">File collection</param>
        private void SetFileCollection(ObservableCollection<string> list)
        {
            if (list != null)
            {
                if (this.fileList == null)
                {
                    this.fileList = list;
                    this.fileList.CollectionChanged += this.FileList_CollectionChanged;

                    foreach (string file in this.RecentFiles)
                    {
                        // Create menu item for each file
                        int count = this.Items.Count;
                        string text = string.Format("{0,2}: {1}", count + 1, file);

                        MenuItem item = new MenuItem { Header = text, Tag = file };
                        item.Click += (s, e) => { this.OnRecentFileClick(((MenuItem)s).Tag as string); };

                        this.Items.Insert(count++, item);
                    }
                }
            }
            else
            {
                // Incoming list is null - if existing file list is not null, clear event handle and items
                if (this.fileList != null)
                {
                    this.fileList.CollectionChanged -= this.FileList_CollectionChanged;
                    this.Items.Clear();
                }
            }
        }

        /// <summary>
        /// Handles the <see cref="CollectionChanged"/> event of the file list.
        /// </summary>
        /// <param name="sender">Event sender</param>
        /// <param name="e">Event arguments</param>
        private void FileList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Reset:
                    this.Items.Clear();
                    break;

                case NotifyCollectionChangedAction.Add:
                    if (e.NewItems != null)
                    {
                        foreach (string file in e.NewItems.Cast<string>())
                        {
                            int count = this.Items.Count;

                            // Create menu item for each file
                            string text = string.Format("{0,2}: {1}", count + 1, file);

                            MenuItem item = new MenuItem { Header = text, Tag = file };
                            item.Click += (s, ev) => { this.OnRecentFileClick(((MenuItem)s).Tag as string); };

                            this.Items.Insert(count++, item);
                        }
                    }

                    break;

                case NotifyCollectionChangedAction.Remove:
                    IList<string> oldItems = e.OldItems as IList<string>;
                    if (oldItems != null)
                    {
                        foreach (string item in oldItems)
                        { this.Items.Remove(item); }
                    }

                    break;

                case NotifyCollectionChangedAction.Move:
                    string movedFile = this.Items[e.OldStartingIndex] as string;
                    this.Items.RemoveAt(e.OldStartingIndex);
                    this.Items.Insert(e.NewStartingIndex, movedFile);
                    break;
            }
        }
    }
}
