﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;

namespace Honu.LogViewer.App.Controls
{
    /// <summary>
    /// Adorned Content Control
    /// </summary>
    public class AdornedContentControl : ContentControl
    {
        /// <summary>
        /// Identifies the <see cref="AdornerContent"/> dependency property
        /// </summary>
        public static readonly DependencyProperty AdornerContentProperty = DependencyProperty.Register(
            "AdornerContent",
            typeof(FrameworkElement),
            typeof(AdornedContentControl),
            new FrameworkPropertyMetadata(AdornedContentControl.AdornerContent_PropertyChanged));

        /// <summary>
        /// Identifies the <see cref="AdornerOffsetX"/> dependency property
        /// </summary>
        public static readonly DependencyProperty AdornerOffsetXProperty = DependencyProperty.Register(
            "AdornerOffsetX",
            typeof(double),
            typeof(AdornedContentControl));

        /// <summary>
        /// Identifies the <see cref="AdornerOffsetY"/> dependency property
        /// </summary>
        public static readonly DependencyProperty AdornerOffsetYProperty = DependencyProperty.Register(
            "AdornerOffsetY",
            typeof(double),
            typeof(AdornedContentControl));

        /// <summary>
        /// Identifies the <see cref="HideAdorner"/> dependency property
        /// </summary>
        public static readonly RoutedCommand HideAdornerCommand = new RoutedCommand("HideAdorner", typeof(AdornedContentControl));

        /// <summary>
        /// Identifies the <see cref="HorizontalAdornerPlacement"/> dependency property
        /// </summary>
        public static readonly DependencyProperty HorizontalAdornerPlacementProperty = DependencyProperty.Register(
            "HorizontalAdornerPlacement",
            typeof(AdornerPlacement),
            typeof(AdornedContentControl),
            new FrameworkPropertyMetadata(AdornerPlacement.Inside));

        /// <summary>
        /// Identifies the <see cref="IsAdornerVisible"/> dependency property
        /// </summary>
        public static readonly DependencyProperty IsAdornerVisibleProperty = DependencyProperty.Register(
            "IsAdornerVisible",
            typeof(bool),
            typeof(AdornedContentControl),
            new FrameworkPropertyMetadata(false, AdornedContentControl.IsAdornerVisible_PropertyChanged));

        /// <summary>
        /// Identifies the <see cref="ShowAdorner"/> dependency property
        /// </summary>
        public static readonly RoutedCommand ShowAdornerCommand = new RoutedCommand("ShowAdorner", typeof(AdornedContentControl));

        /// <summary>
        /// Identifies the <see cref="VerticalAdornerPlacement"/> dependency property
        /// </summary>
        public static readonly DependencyProperty VerticalAdornerPlacementProperty = DependencyProperty.Register(
            "VerticalAdornerPlacement",
            typeof(AdornerPlacement),
            typeof(AdornedContentControl),
            new FrameworkPropertyMetadata(AdornerPlacement.Inside));

        /// <summary>
        /// The Hide Adorner command binding
        /// </summary>
        private static readonly CommandBinding HideAdornerCommandBinding = new CommandBinding(AdornedContentControl.HideAdornerCommand, AdornedContentControl.HideAdornerCommand_Executed);

        /// <summary>
        /// The Show Adorner command binding
        /// </summary>
        private static readonly CommandBinding ShowAdornerCommandBinding = new CommandBinding(AdornedContentControl.ShowAdornerCommand, AdornedContentControl.ShowAdornerCommand_Executed);

        /// <summary>
        /// The actual adorner create to contain our 'adorner UI content'.
        /// </summary>
        private FrameworkElementAdorner adorner;

        /// <summary>
        /// Caches the adorner layer.
        /// </summary>
        private AdornerLayer adornerLayer;

        /// <summary>
        /// Initializes static members of the <see cref="AdornedContentControl"/> class.
        /// </summary>
        static AdornedContentControl()
        {
            CommandManager.RegisterClassCommandBinding(typeof(AdornedContentControl), AdornedContentControl.ShowAdornerCommandBinding);
            CommandManager.RegisterClassCommandBinding(typeof(AdornedContentControl), AdornedContentControl.HideAdornerCommandBinding);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AdornedContentControl"/> class.
        /// </summary>
        public AdornedContentControl()
        { this.DataContextChanged += this.AdornedControl_DataContextChanged; }

        /// <summary>
        /// Gets or sets the <see cref="FrameworkElement"/> used to define the UI content of the adorner.
        /// </summary>
        public FrameworkElement AdornerContent
        {
            get { return this.GetValue(AdornedContentControl.AdornerContentProperty) as FrameworkElement; }
            set { this.SetValue(AdornedContentControl.AdornerContentProperty, value); }
        }

        /// <summary>
        /// Gets or sets the X offset of the adorner.
        /// </summary>
        public double AdornerOffsetX
        {
            get { return (double)this.GetValue(AdornedContentControl.AdornerOffsetXProperty); }
            set { this.SetValue(AdornedContentControl.AdornerOffsetXProperty, value); }
        }

        /// <summary>
        /// Gets or sets the Y offset of the adorner.
        /// </summary>
        public double AdornerOffsetY
        {
            get { return (double)this.GetValue(AdornedContentControl.AdornerOffsetYProperty); }
            set { this.SetValue(AdornedContentControl.AdornerOffsetYProperty, value); }
        }

        /// <summary>
        /// Gets or sets the horizontal placement of the adorner relative to the adorned control.
        /// </summary>
        public AdornerPlacement HorizontalAdornerPlacement
        {
            get { return (AdornerPlacement)this.GetValue(AdornedContentControl.HorizontalAdornerPlacementProperty); }
            set { this.SetValue(AdornedContentControl.HorizontalAdornerPlacementProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show the adorner.
        /// </summary>
        public bool IsAdornerVisible
        {
            get { return (bool)this.GetValue(AdornedContentControl.IsAdornerVisibleProperty); }
            set { this.SetValue(AdornedContentControl.IsAdornerVisibleProperty, value); }
        }

        /// <summary>
        /// Gets or sets the vertical placement of the adorner relative to the adorned control.
        /// </summary>
        public AdornerPlacement VerticalAdornerPlacement
        {
            get { return (AdornerPlacement)this.GetValue(AdornedContentControl.VerticalAdornerPlacementProperty); }
            set { this.SetValue(AdornedContentControl.VerticalAdornerPlacementProperty, value); }
        }

        /// <summary>
        /// Hide the adorner.
        /// </summary>
        public void HideAdorner()
        { this.IsAdornerVisible = false; }

        /// <summary>
        /// Shows the adorner.
        /// </summary>
        public void ShowAdorner()
        { this.IsAdornerVisible = true; }

        /// <summary>
        /// When overridden in a derived class, is invoked whenever application code or internal processes call <see cref="M:System.Windows.FrameworkElement.ApplyTemplate" />.
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this.ShowOrHideAdornerInternal();
        }

        /// <summary>
        /// Occurs when the <see cref="AdornerContent"/> property has changed
        /// </summary>
        /// <param name="o">The o.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void AdornerContent_PropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            AdornedContentControl c = o as AdornedContentControl;
            if (c != null)
            { c.ShowOrHideAdornerInternal(); }
        }

        /// <summary>
        /// Handles the Executed event of the HideAdornerCommand control.
        /// </summary>
        /// <param name="target">The source of the event.</param>
        /// <param name="e">The <see cref="ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private static void HideAdornerCommand_Executed(object target, ExecutedRoutedEventArgs e)
        {
            AdornedContentControl c = target as AdornedContentControl;
            if (c != null)
            { c.HideAdorner(); }
        }

        /// <summary>
        /// Event raised when the value of IsAdornerVisible has changed.
        /// </summary>
        /// <param name="o">The o.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private static void IsAdornerVisible_PropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            AdornedContentControl c = o as AdornedContentControl;
            if (c != null)
            { c.ShowOrHideAdornerInternal(); }
        }

        /// <summary>
        /// Handles the Executed event of the ShowAdornerCommand control.
        /// </summary>
        /// <param name="target">The source of the event.</param>
        /// <param name="e">The <see cref="ExecutedRoutedEventArgs"/> instance containing the event data.</param>
        private static void ShowAdornerCommand_Executed(object target, ExecutedRoutedEventArgs e)
        {
            AdornedContentControl c = target as AdornedContentControl;
            if (c != null)
            { c.ShowAdorner(); }
        }

        /// <summary>
        /// Handles the DataContextChanged event of the AdornedControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private void AdornedControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            this.UpdateAdornerDataContext();
        }

        /// <summary>
        /// Internal method to hide the adorner.
        /// </summary>
        private void HideAdornerInternal()
        {
            if (this.adornerLayer == null || this.adorner == null)
            {
                // Not already adorned.
                return;
            }

            this.adornerLayer.Remove(this.adorner);
            this.adorner.DisconnectChild();

            this.adorner = null;
            this.adornerLayer = null;
        }

        /// <summary>
        /// Internal method to show the adorner.
        /// </summary>
        private void ShowAdornerInternal()
        {
            // Already adorned.
            if (this.adorner != null)
            { return; }

            if (this.AdornerContent != null)
            {
                // Create adorner layer if necessary
                if (this.adornerLayer == null)
                { this.adornerLayer = AdornerLayer.GetAdornerLayer(this); }

                // Instantiate adorner
                if (this.adornerLayer != null)
                {
                    this.adorner = new FrameworkElementAdorner(
                        this,
                        this.AdornerContent,
                        this.HorizontalAdornerPlacement,
                        this.VerticalAdornerPlacement,
                        this.AdornerOffsetX,
                        this.AdornerOffsetY);

                    this.adornerLayer.Add(this.adorner);
                    this.UpdateAdornerDataContext();
                }
            }
        }

        /// <summary>
        /// Internal method to show or hide the adorner based on the value of IsAdornerVisible.
        /// </summary>
        private void ShowOrHideAdornerInternal()
        {
            if (this.IsAdornerVisible)
            { this.ShowAdornerInternal(); }
            else
            { this.HideAdornerInternal(); }
        }

        /// <summary>
        /// Update the DataContext of the adorner from the adorned control.
        /// </summary>
        private void UpdateAdornerDataContext()
        {
            if (this.AdornerContent != null)
            { this.AdornerContent.DataContext = this.DataContext; }
        }
    }
}
