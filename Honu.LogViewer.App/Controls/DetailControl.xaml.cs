﻿using System.Windows.Controls;

namespace Honu.LogViewer.App.Controls
{
    /// <summary>
    /// Interaction logic for <see cref="DetailControl"/>
    /// </summary>
    internal partial class DetailControl : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DetailControl"/> class.
        /// </summary>
        public DetailControl()
        { this.InitializeComponent(); }
    }
}
