﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Honu.LogViewer.App.Controls
{
    /// <summary>
    /// Interaction logic for <see cref="LoadingControl"/>
    /// </summary>
    public partial class LoadingControl : UserControl
    {
        /// <summary>
        /// Animation timer
        /// </summary>
        private readonly DispatcherTimer animationTimer;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoadingControl"/> class.
        /// </summary>
        public LoadingControl()
        {
            this.InitializeComponent();

            this.animationTimer = new DispatcherTimer(DispatcherPriority.ContextIdle, Dispatcher) { Interval = new TimeSpan(0, 0, 0, 0, 75) };
            this.animationTimer.Tick += this.HandleAnimationTick;
        }

        /// <summary>
        /// Sets the position.
        /// </summary>
        /// <param name="ellipse">The ellipse.</param>
        /// <param name="offset">The offset.</param>
        /// <param name="posOffSet">The position off set.</param>
        /// <param name="step">The step.</param>
        private static void SetPosition(Ellipse ellipse, double offset, double posOffSet, double step)
        {
            double stepOffset = offset + (posOffSet * step);
            ellipse.SetValue(Canvas.LeftProperty, 50.0 + (Math.Sin(stepOffset) * 50.0));
            ellipse.SetValue(Canvas.TopProperty, 50 + (Math.Cos(stepOffset) * 50.0));
        }

        /// <summary>
        /// Starts the animation.
        /// </summary>
        private void Start()
        {
            Mouse.OverrideCursor = Cursors.Wait;
            this.animationTimer.Start();
        }

        /// <summary>
        /// Stops the animation.
        /// </summary>
        private void Stop()
        {
            this.animationTimer.Stop();
            Mouse.OverrideCursor = Cursors.Arrow;
        }

        /// <summary>
        /// Handles the animation tick event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void HandleAnimationTick(object sender, EventArgs e)
        { this.SpinnerRotate.Angle = (this.SpinnerRotate.Angle + 36) % 360; }

        /// <summary>
        /// Handles the loaded event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void HandleLoaded(object sender, RoutedEventArgs e)
        {
            const double Offset = Math.PI;
            const double Step = Math.PI * 2 / 10.0;

            LoadingControl.SetPosition(this.C0, Offset, 0.0, Step);
            LoadingControl.SetPosition(this.C1, Offset, 1.0, Step);
            LoadingControl.SetPosition(this.C2, Offset, 2.0, Step);
            LoadingControl.SetPosition(this.C3, Offset, 3.0, Step);
            LoadingControl.SetPosition(this.C4, Offset, 4.0, Step);
            LoadingControl.SetPosition(this.C5, Offset, 5.0, Step);
            LoadingControl.SetPosition(this.C6, Offset, 6.0, Step);
            LoadingControl.SetPosition(this.C7, Offset, 7.0, Step);
            LoadingControl.SetPosition(this.C8, Offset, 8.0, Step);
        }

        /// <summary>
        /// Handles the unloaded event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs"/> instance containing the event data.</param>
        private void HandleUnloaded(object sender, RoutedEventArgs e)
        { this.Stop(); }

        /// <summary>
        /// Handles the visible changed event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        private void HandleVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            bool isVisible = (bool)e.NewValue;

            if (isVisible)
            { this.Start(); }
            else
            { this.Stop(); }
        }
    }
}
