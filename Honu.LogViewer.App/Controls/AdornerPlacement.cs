﻿namespace Honu.LogViewer.App.Controls
{
    /// <summary>
    /// Specifies the placement of the adorner in related to the adorned control.
    /// </summary>
    public enum AdornerPlacement
    {
        /// <summary>
        /// Inside placement
        /// </summary>
        Inside,

        /// <summary>
        /// Outside placement
        /// </summary>
        Outside
    }
}
