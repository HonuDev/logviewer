﻿using System;
using System.Collections.Generic;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Honu.LogViewer.App.Xml
{
    /// <summary>
    /// Recently Used Files
    /// </summary>
    [Serializable]
    [XmlType(TypeName = "RecentFiles")]
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class RecentFilesElement
    {
        /// <summary>
        /// List of <see cref="RecentFileElement"/>
        /// </summary>
        private List<RecentFileElement> files;

        /// <summary>
        /// Initializes a new instance of the <see cref="RecentFilesElement"/> class.
        /// </summary>
        public RecentFilesElement()
        { this.files = new List<RecentFileElement>(); }

        /// <summary>
        /// Gets or sets the collection of recently used files.
        /// </summary>
        [XmlElement("RecentFile", Form = XmlSchemaForm.Unqualified)]
        public List<RecentFileElement> Files
        {
            get { return this.files ?? new List<RecentFileElement>(); }
            set { this.files = value; }
        }
    }
}
