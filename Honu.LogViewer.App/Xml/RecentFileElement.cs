﻿using System;
using System.Diagnostics;
using System.Xml.Serialization;

namespace Honu.LogViewer.App.Xml
{
    /// <summary>
    /// Recently Used File
    /// </summary>
    [Serializable]
    [DebuggerDisplay("{Path}")]
    [XmlType(TypeName = "RecentFile")]
    public class RecentFileElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RecentFileElement"/> class.
        /// </summary>
        public RecentFileElement() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="RecentFileElement"/> class.
        /// </summary>
        /// <param name="path">A file path</param>
        public RecentFileElement(string path)
        { this.Path = path; }

        /// <summary>
        /// Gets or sets the path of the recently used file
        /// </summary>
        [XmlAttribute("path")]
        public string Path
        { get; set; }
    }
}
