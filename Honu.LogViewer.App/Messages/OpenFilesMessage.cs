﻿using System.Collections.Generic;
using GalaSoft.MvvmLight.Messaging;

namespace Honu.LogViewer.App.Messages
{
    /// <summary>
    /// Open Files Message
    /// </summary>
    /// <seealso cref="GalaSoft.MvvmLight.Messaging.GenericMessage{System.Collections.Generic.IEnumerable{System.String}}" />
    internal class OpenFilesMessage : GenericMessage<IEnumerable<string>>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenFilesMessage"/> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public OpenFilesMessage(IEnumerable<string> content)
            : base(content)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenFilesMessage"/> class.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="content">The content.</param>
        public OpenFilesMessage(object sender, IEnumerable<string> content)
            : base(sender, content)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenFilesMessage"/> class.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="target">The target.</param>
        /// <param name="content">The content.</param>
        public OpenFilesMessage(object sender, object target, IEnumerable<string> content)
            : base(sender, target, content)
        { }
    }
}
