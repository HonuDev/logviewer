﻿using GalaSoft.MvvmLight.Messaging;

namespace Honu.LogViewer.App.Messages
{
    /// <summary>
    /// Go To Log Message
    /// </summary>
    /// <seealso cref="GalaSoft.MvvmLight.Messaging.GenericMessage{System.Int32}" />
    internal sealed class GoToLogMessage : GenericMessage<int>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GoToLogMessage"/> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public GoToLogMessage(int content)
            : base(content)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="GoToLogMessage"/> class.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="content">The content.</param>
        public GoToLogMessage(object sender, int content)
            : base(sender, content)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="GoToLogMessage"/> class.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="target">The target.</param>
        /// <param name="content">The content.</param>
        public GoToLogMessage(object sender, object target, int content)
            : base(sender, target, content)
        { }
    }
}
