﻿using GalaSoft.MvvmLight.Messaging;

namespace Honu.LogViewer.App.Messages
{
    /// <summary>
    /// Show Filter Message
    /// </summary>
    /// <seealso cref="GalaSoft.MvvmLight.Messaging.MessageBase" />
    internal class ShowFilterMessage : MessageBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShowFilterMessage"/> class.
        /// </summary>
        public ShowFilterMessage()
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShowFilterMessage"/> class.
        /// </summary>
        /// <param name="sender">The message's original sender.</param>
        public ShowFilterMessage(object sender)
            : base(sender)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShowFilterMessage"/> class.
        /// </summary>
        /// <param name="sender">The message's original sender.</param>
        /// <param name="target">The message's intended target. This parameter can be used
        /// to give an indication as to whom the message was intended for. Of course
        /// this is only an indication, and may be null.</param>
        public ShowFilterMessage(object sender, object target)
            : base(sender, target)
        { }
    }
}
