﻿using System.Windows;
using GalaSoft.MvvmLight.Messaging;

namespace Honu.LogViewer.App.Messages
{
    /// <summary>
    /// Close Window Message
    /// </summary>
    /// <seealso cref="GalaSoft.MvvmLight.Messaging.GenericMessage{System.Windows.Window}" />
    internal sealed class CloseWindowMessage : GenericMessage<Window>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CloseWindowMessage"/> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public CloseWindowMessage(Window content)
            : base(content)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="CloseWindowMessage"/> class.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="content">The content.</param>
        public CloseWindowMessage(object sender, Window content)
            : base(sender, content)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="CloseWindowMessage"/> class.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="target">The target.</param>
        /// <param name="content">The content.</param>
        public CloseWindowMessage(object sender, object target, Window content)
            : base(sender, target, content)
        { }
    }
}
