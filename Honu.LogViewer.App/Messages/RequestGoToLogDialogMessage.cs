﻿using System.Collections.Generic;
using GalaSoft.MvvmLight.Messaging;

namespace Honu.LogViewer.App.Messages
{
    /// <summary>
    /// Request Go To Log Dialog Message
    /// </summary>
    /// <seealso cref="GalaSoft.MvvmLight.Messaging.GenericMessage{System.Collections.Generic.IEnumerable{System.Int32}}" />
    internal sealed class RequestGoToLogDialogMessage : GenericMessage<IEnumerable<int>>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequestGoToLogDialogMessage"/> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public RequestGoToLogDialogMessage(IEnumerable<int> content)
            : base(content)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestGoToLogDialogMessage"/> class.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="content">The content.</param>
        public RequestGoToLogDialogMessage(object sender, IEnumerable<int> content)
            : base(sender, content)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestGoToLogDialogMessage"/> class.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="target">The target.</param>
        /// <param name="content">The content.</param>
        public RequestGoToLogDialogMessage(object sender, object target, IEnumerable<int> content)
            : base(sender, target, content)
        { }
    }
}
