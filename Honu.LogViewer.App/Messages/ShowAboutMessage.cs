﻿using GalaSoft.MvvmLight.Messaging;

namespace Honu.LogViewer.App.Messages
{
    /// <summary>
    /// Show About Window Message
    /// </summary>
    /// <seealso cref="GalaSoft.MvvmLight.Messaging.MessageBase" />
    internal sealed class ShowAboutMessage : MessageBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShowAboutMessage"/> class.
        /// </summary>
        public ShowAboutMessage()
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShowAboutMessage"/> class.
        /// </summary>
        /// <param name="sender">The message's original sender.</param>
        public ShowAboutMessage(object sender)
            : base(sender)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShowAboutMessage"/> class.
        /// </summary>
        /// <param name="sender">The message's original sender.</param>
        /// <param name="target">The message's intended target. This parameter can be used
        /// to give an indication as to whom the message was intended for. Of course
        /// this is only an indication, and may be null.</param>
        public ShowAboutMessage(object sender, object target)
            : base(sender, target)
        { }
    }
}
