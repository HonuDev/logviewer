﻿using GalaSoft.MvvmLight.Messaging;

namespace Honu.LogViewer.App.Messages
{
    /// <summary>
    /// Open File Dialog Message
    /// </summary>
    /// <seealso cref="GalaSoft.MvvmLight.Messaging.MessageBase" />
    internal class RequestOpenFileDialogMessage : MessageBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RequestOpenFileDialogMessage"/> class.
        /// </summary>
        public RequestOpenFileDialogMessage()
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestOpenFileDialogMessage"/> class.
        /// </summary>
        /// <param name="sender">The message's original sender.</param>
        public RequestOpenFileDialogMessage(object sender)
            : base(sender)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestOpenFileDialogMessage"/> class.
        /// </summary>
        /// <param name="sender">The message's original sender.</param>
        /// <param name="target">The message's intended target. This parameter can be used
        /// to give an indication as to whom the message was intended for. Of course
        /// this is only an indication, and may be null.</param>
        public RequestOpenFileDialogMessage(object sender, object target)
            : base(sender, target)
        { }
    }
}
