﻿using System.IO;
using GalaSoft.MvvmLight.Messaging;

namespace Honu.LogViewer.App.Messages
{
    /// <summary>
    /// Open Stream Message
    /// </summary>
    /// <seealso cref="GalaSoft.MvvmLight.Messaging.GenericMessage{System.IO.Stream}" />
    internal class OpenStreamMessage : GenericMessage<Stream>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OpenStreamMessage"/> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public OpenStreamMessage(Stream content)
            : base(content)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenStreamMessage"/> class.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="content">The content.</param>
        public OpenStreamMessage(object sender, Stream content)
            : base(sender, content)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="OpenStreamMessage"/> class.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="target">The target.</param>
        /// <param name="content">The content.</param>
        public OpenStreamMessage(object sender, object target, Stream content)
            : base(sender, target, content)
        { }
    }
}
