﻿using System.Collections.Generic;
using GalaSoft.MvvmLight.Messaging;

namespace Honu.LogViewer.App.Messages
{
    /// <summary>
    /// Logs Retrieved Message
    /// </summary>
    /// <seealso cref="GalaSoft.MvvmLight.Messaging.GenericMessage{System.Collections.Generic.IEnumerable{Honu.LogViewer.App.ClientLogEntry}}" />
    internal class LogsRetrievedMessage : GenericMessage<IEnumerable<ClientLogEntry>>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LogsRetrievedMessage"/> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public LogsRetrievedMessage(IEnumerable<ClientLogEntry> content)
            : base(content)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogsRetrievedMessage"/> class.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="content">The content.</param>
        public LogsRetrievedMessage(object sender, IEnumerable<ClientLogEntry> content)
            : base(sender, content)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogsRetrievedMessage"/> class.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="target">The target.</param>
        /// <param name="content">The content.</param>
        public LogsRetrievedMessage(object sender, object target, IEnumerable<ClientLogEntry> content)
            : base(sender, target, content)
        { }
    }
}