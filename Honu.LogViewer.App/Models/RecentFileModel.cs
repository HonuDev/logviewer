﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Security.Permissions;
using System.Xml;
using System.Xml.Serialization;
using GalaSoft.MvvmLight;
using Honu.LogViewer.App.Xml;
using log4net;

namespace Honu.LogViewer.App.Models
{
    /// <summary>
    /// Recent File Model
    /// </summary>
    /// <seealso cref="GalaSoft.MvvmLight.ObservableObject" />
    internal class RecentFileModel : ObservableObject
    {
        /// <summary>
        /// <see cref="RecentFileModel"/> logger
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(RecentFileModel));

        /// <summary>
        /// Recent files
        /// </summary>
        private readonly ObservableCollection<string> recentFiles;

        /// <summary>
        /// Initializes a new instance of the <see cref="RecentFileModel"/> class.
        /// </summary>
        public RecentFileModel()
        {
            if (!ViewModelBase.IsInDesignModeStatic)
            {
                // Retrieve application information
                string company = ApplicationAttributes.CompanyName;
                string product = ApplicationAttributes.ProductName;

                // Create file path
                this.FilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), company, product, "RecentFiles.xml");

                Log.InfoFormat("Recent File XML set to '{0}'", this.FilePath);
                this.recentFiles = new ObservableCollection<string>();
                this.LoadRecentFiles();
            }
        }

        /// <summary>
        /// Gets the collection of recently-used files
        /// </summary>
        public ObservableCollection<string> RecentFiles
        { get { return this.recentFiles; } }

        /// <summary>
        /// Gets or sets the file path of the recent file list
        /// </summary>
        private string FilePath
        { get; set; }

        /// <summary>
        /// Adds the specified path to the recently-used service
        /// </summary>
        /// <param name="path">The path of the file to be added</param>
        public void Add(string path)
        { this.Add(new List<string> { path }); }

        /// <summary>
        /// Adds the specified paths to the recently-used service
        /// </summary>
        /// <param name="paths">The collection of files to be added</param>
        public void Add(IEnumerable<string> paths)
        {
            // Check for errors
            if (paths == null)
            { throw new ArgumentNullException("paths"); }
            if (paths.Count().Equals(0))
            { throw new ArgumentException("Paths collection cannot be empty"); }

            Log.DebugFormat("Adding files '{0}'", string.Join(", ", paths));

            // Get lists
            List<string> newList = new List<string>(paths);

            if (this.RecentFiles != null)
            {
                // Build filter to eliminate null values, duplicates, missing and networked files
                IEnumerable<string> fileList = this.RecentFiles.Distinct().Where(s => !string.IsNullOrWhiteSpace(s) && File.Exists(s));

                // Build new list
                foreach (string file in fileList.Where(f => !newList.Contains(f)))
                { newList.Add(file); }
            }

            // Add to recent used list
            this.RecentFiles.Clear();
            newList.ForEach(this.RecentFiles.Add);

            // Serialize back to output file
            try
            {
                Log.Debug("Creating new RecentFiles document");
                RecentFilesElement element = new RecentFilesElement { Files = this.RecentFiles.Select(l => new RecentFileElement(l)).ToList() };
                XmlSerializer serializer = new XmlSerializer(typeof(RecentFilesElement));

                Log.DebugFormat("Writing new RecentFiles document to path '{0}'", this.FilePath);
                using (TextWriter writer = new StreamWriter(this.FilePath, false))
                {
                    serializer.Serialize(writer, element);
                    writer.Flush();
                }
            }
            catch (Exception ex)
            { Log.Error(ex.Message, ex); }
        }

        /// <summary>
        /// Verifies that the specified path is local
        /// </summary>
        /// <param name="path">File path</param>
        /// <returns>true if the path is local</returns>
        private static bool IsLocalFile(string path)
        {
            // Get host addresses
            IPAddress[] host;

            try
            {
                Uri uri = new Uri(path);
                host = Dns.GetHostAddresses(uri.Host);
            }
            catch
            { return false; }

            // Check if local
            IPAddress[] local = Dns.GetHostAddresses(Dns.GetHostName());
            return host.Any(a => IPAddress.IsLoopback(a) || local.Contains(a));
        }

        /// <summary>
        /// Loads the collection of recently loaded files.
        /// </summary>
        private void LoadRecentFiles()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(this.FilePath))
                {
                    Log.InfoFormat("'{0}' is not a valid path", this.FilePath);
                    return;
                }

                FileInfo file = new FileInfo(this.FilePath);
                if (!file.Directory.Exists)
                {
                    Log.InfoFormat("Creating Recently Used File directory '{0}'", file.Directory.FullName);
                    file.Directory.Create();
                    return;
                }

                if (!file.Exists)
                {
                    Log.InfoFormat("Recently Used File '{0}' does not exist", this.FilePath);
                    return;
                }

                Log.Debug("Requesting Read access for file");
                FileIOPermission access = new FileIOPermission(FileIOPermissionAccess.Read, this.FilePath);
                access.Demand();
            }
            catch (SecurityException se)
            {
                Log.Error(se.Message, se);
                return;
            }

            // Open reader
            RecentFilesElement recentFiles = null;
            using (FileStream stream = new FileStream(this.FilePath, FileMode.OpenOrCreate, FileAccess.Read, FileShare.ReadWrite))
            {
                // Check for deserialization capability
                XmlSerializer serializer = new XmlSerializer(typeof(RecentFilesElement));
                if (!serializer.CanDeserialize(XmlReader.Create(stream)))
                {
                    Log.WarnFormat("Cannot deserialize Recently Used file list at path '{0}'", this.FilePath);
                    return;
                }

                // Reset stream position and deserialize
                stream.Position = 0;
                recentFiles = serializer.Deserialize(stream) as RecentFilesElement;
            }

            // Check for deserialization
            if (recentFiles == null | recentFiles.Files == null)
            {
                Log.Warn("Unable to deserialize recently used file");
                return;
            }

            // Build recent files list
            recentFiles.Files.ForEach(file =>
            {
                if (!string.IsNullOrWhiteSpace(file.Path) && RecentFileModel.IsLocalFile(file.Path) && File.Exists(file.Path))
                { this.RecentFiles.Add(file.Path); }
            });
        }
    }
}