﻿using System.Diagnostics.CodeAnalysis;
using System.Windows.Media;
using GalaSoft.MvvmLight;
using Honu.LogViewer.App.Properties;
using log4net;

namespace Honu.LogViewer.App.Models
{
    /// <summary>
    /// Settings Model
    /// </summary>
    /// <seealso cref="GalaSoft.MvvmLight.ObservableObject" />
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:ElementsMustBeDocumented", Justification = "Reviewed.")]
    internal class SettingsModel : ObservableObject
    {
        /// <summary>
        /// <see cref="SettingsModel"/> logger
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(SettingsModel));

        // Original colors
        private Color originalDebug;
        private Color originalInfo;
        private Color originalWarn;
        private Color originalError;
        private Color originalFatal;
        private Color originalUnknown;

        // Proposed colors
        private Color debug;
        private Color info;
        private Color warn;
        private Color error;
        private Color fatal;
        private Color unknown;

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsModel"/> class.
        /// </summary>
        public SettingsModel()
        {
            // Store original colors
            this.originalDebug = Settings.Default.DebugColor;
            this.originalInfo = Settings.Default.InfoColor;
            this.originalWarn = Settings.Default.WarnColor;
            this.originalError = Settings.Default.ErrorColor;
            this.originalFatal = Settings.Default.FatalColor;
            this.originalUnknown = Settings.Default.UnknownColor;

            // Set model colors
            this.SetOriginalColors();
        }

        /// <summary>
        /// Gets or sets the Debug color
        /// </summary>
        public Color Debug
        {
            get { return this.debug; }
            set { this.Set<Color>(() => this.Debug, ref this.debug, value); }
        }

        /// <summary>
        /// Gets or sets the Info color
        /// </summary>
        public Color Info
        {
            get { return this.info; }
            set { this.Set<Color>(() => this.Info, ref this.info, value); }
        }

        /// <summary>
        /// Gets or sets the Warning color
        /// </summary>
        public Color Warn
        {
            get { return this.warn; }
            set { this.Set<Color>(() => this.Warn, ref this.warn, value); }
        }

        /// <summary>
        /// Gets or sets the Error color
        /// </summary>
        public Color Error
        {
            get { return this.error; }
            set { this.Set<Color>(() => this.Error, ref this.error, value); }
        }

        /// <summary>
        /// Gets or sets the Fatal color
        /// </summary>
        public Color Fatal
        {
            get { return this.fatal; }
            set { this.Set<Color>(() => this.Fatal, ref this.fatal, value); }
        }

        /// <summary>
        /// Gets or sets the Unknown color
        /// </summary>
        public Color Unknown
        {
            get { return this.unknown; }
            set { this.Set<Color>(() => this.Unknown, ref this.unknown, value); }
        }

        /// <summary>
        /// Gets a value indicating whether this model has been modified from original settings.
        /// </summary>
        public bool IsModified
        {
            get
            {
                return this.Debug != this.originalDebug ||
                    this.Info != this.originalInfo ||
                    this.Warn != this.originalWarn ||
                    this.Error != this.originalError ||
                    this.Fatal != this.originalFatal ||
                    this.Unknown != this.originalUnknown;
            }
        }

        /// <summary>
        /// Commits the changes.
        /// </summary>
        public void CommitChanges()
        {
            if (!this.IsModified)
            {
                Log.Info("No changes to commit");
                return;
            }

            // Apply settings
            Log.DebugFormat("Setting Debug color: {0}", this.Debug);
            Settings.Default.DebugColor = this.Debug;

            Log.DebugFormat("Setting Info color: {0}", this.Info);
            Settings.Default.InfoColor = this.Info;

            Log.DebugFormat("Setting Warn color: {0}", this.Warn);
            Settings.Default.WarnColor = this.Warn;

            Log.DebugFormat("Setting Error color: {0}", this.Error);
            Settings.Default.ErrorColor = this.Error;

            Log.DebugFormat("Setting Fatal color: {0}", this.Fatal);
            Settings.Default.FatalColor = this.Fatal;

            Log.DebugFormat("Setting Unknown color: {0}", this.Unknown);
            Settings.Default.UnknownColor = this.Unknown;

            Log.Info("Saving new color palette");
            Settings.Default.Save();

            // Update originals
            this.originalDebug = this.Debug;
            this.originalInfo = this.Info;
            this.originalWarn = this.Warn;
            this.originalError = this.Error;
            this.originalFatal = this.Fatal;
            this.originalUnknown = this.Unknown;
        }

        /// <summary>
        /// Resets this instance.
        /// </summary>
        public void Reset()
        {
            if (!this.IsModified)
            {
                Log.Info("No changes to reset");
                return;
            }

            this.SetOriginalColors();
        }

        /// <summary>
        /// Sets the original colors.
        /// </summary>
        private void SetOriginalColors()
        {
            this.Debug = this.originalDebug;
            this.Info = this.originalInfo;
            this.Warn = this.originalWarn;
            this.Error = this.originalError;
            this.Fatal = this.originalFatal;
            this.Unknown = this.originalUnknown;
        }
    }
}
