﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using log4net;

namespace Honu.LogViewer.App.Models
{
    /// <summary>
    /// Log Model
    /// </summary>
    /// <seealso cref="GalaSoft.MvvmLight.ObservableObject" />
    internal class LogModel : ObservableObject
    {
        /// <summary>
        /// <see cref="LogModel"/> logger
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger(typeof(LogModel));

        /// <summary>
        /// Initializes a new instance of the <see cref="LogModel"/> class.
        /// </summary>
        public LogModel()
        {
            this.Readers = new List<ILogEntryReader>
            {
                new Log4Net.Log4NetReader(),
                new Log4J.Log4JReader()
            };
        }

        /// <summary>
        /// Gets or sets the collection of <see cref="ILogEntryReader"/> used to load entries
        /// </summary>
        private IEnumerable<ILogEntryReader> Readers
        { get; set; }

        /// <summary>
        /// Retrieves the logs asynchronously.
        /// </summary>
        /// <param name="stream">The stream to read the logs.</param>
        /// <returns>The collection of log entries.</returns>
        public async Task<IEnumerable<ClientLogEntry>> RetrieveLogsAsync(Stream stream)
        {
            if (stream == null)
            { throw new ArgumentNullException("stream"); }

            var result = await this.GetLogsFromStreamAsync(stream);
            if (result != null)
            {
                int logCount = 0;
                List<ClientLogEntry> logs = new List<ClientLogEntry>();

                foreach (ILogEntry entry in result)
                {
                    ClientLogEntry clientEntry = new ClientLogEntry(entry, ++logCount);
                    logs.Add(clientEntry);
                }

                return logs;
            }

            return null;
        }

        /// <summary>
        /// Retrieves the logs asynchronously.
        /// </summary>
        /// <param name="files">The files.</param>
        /// <returns>The collection of log entries.</returns>
        public Task<IEnumerable<ClientLogEntry>> RetrieveLogsAsync(IEnumerable<string> files)
        {
            if (files == null || files.Count() == 0)
            {
                Log.Info("No files were specified");
                return null;
            }

            return Task.Run<IEnumerable<ClientLogEntry>>(async () =>
                {
                    ConcurrentBag<ClientLogEntry> entries = new ConcurrentBag<ClientLogEntry>();
                    var taskList = files.Select(p => this.GetLogsFromFileAsync(p)).ToList();

                    while (taskList.Count > 0)
                    {
                        // Get next completed task
                        Task<IEnumerable<ILogEntry>> completedTask = await Task.WhenAny(taskList);
                        taskList.Remove(completedTask);

                        if (completedTask.Status == TaskStatus.RanToCompletion)
                        {
                            foreach (ILogEntry entry in completedTask.Result)
                            {
                                ClientLogEntry clientEntry = new ClientLogEntry(entry, entries.Count + 1);
                                entries.Add(clientEntry);
                            }
                        }
                    }

                    return entries;
                });
        }

        /// <summary>
        /// Gets the logs from file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>Asynchronous Task</returns>
        private async Task<IEnumerable<ILogEntry>> GetLogsFromFileAsync(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath))
            { return null; }

            FileInfo file = new FileInfo(filePath);
            if (!file.Exists)
            {
                Log.InfoFormat("File '{0}' does not exist", filePath);
                return null;
            }

            try
            {
                // Read file contents
                using (FileStream stream = file.OpenRead())
                {
                    // Since it's difficult to know what files are what type of log - loop readers and try each
                    return await this.GetLogsFromStreamAsync(stream, filePath);
                }
            }
            catch (Exception ex)
            { Log.Error(ex.Message, ex); }

            return null;
        }

        /// <summary>
        /// Gets the logs from stream asynchronously.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="filePath">Optional: The file path.</param>
        /// <returns>Collection of log entries.</returns>
        private async Task<IEnumerable<ILogEntry>> GetLogsFromStreamAsync(Stream stream, string filePath = "")
        {
            // Since it's difficult to know what files are what type of log - loop readers and try each
            foreach (ILogEntryReader reader in this.Readers)
            {
                try
                {
                    // Reset position and read file
                    stream.Position = 0;
                    return await reader.GetLogsAsync(stream, filePath);
                }
                catch
                {
                    if (string.IsNullOrWhiteSpace(filePath))
                    { Log.InfoFormat("Stream is not of log type '{0}'", reader.GetType().Name); }
                    else
                    { Log.InfoFormat("File '{0}' is not of log type '{1}'", filePath, reader.GetType().Name); }
                }
            }

            return null;
        }
    }
}