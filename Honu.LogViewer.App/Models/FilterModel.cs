﻿using System;
using System.Diagnostics.CodeAnalysis;
using GalaSoft.MvvmLight;

namespace Honu.LogViewer.App.Models
{
    /// <summary>
    /// Filter Model
    /// </summary>
    /// <seealso cref="GalaSoft.MvvmLight.ObservableObject" />
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1600:ElementsMustBeDocumented", Justification = "Reviewed.")]
    internal class FilterModel : ObservableObject
    {
        private bool showDebug;
        private bool showInfo;
        private bool showWarn;
        private bool showError;
        private bool showFatal;
        private bool showUnknown;
        private string message;
        private string thread;
        private string machine;
        private string user;
        private string application;
        private string @class;
        private string method;
        private string codeFile;
        private string logger;
        private string host;

        /// <summary>
        /// Initializes a new instance of the <see cref="FilterModel"/> class.
        /// </summary>
        public FilterModel()
        { this.FilterChanged += delegate { this.RaisePropertyChanged(() => this.IsEmpty); }; }

        /// <summary>
        /// Occurs when the filter model has changed.
        /// </summary>
        public event EventHandler<EventArgs> FilterChanged = (sender, e) => { };

        /// <summary>
        /// Occurs when the filter model has been cleared.
        /// </summary>
        public event EventHandler<EventArgs> FilterCleared = (sender, e) => { };

        /// <summary>
        /// Gets or sets a value indicating whether to filter by Debug logs.
        /// </summary>
        public bool DebugLevel
        {
            get { return this.showDebug; }
            set
            {
                if (this.Set(() => this.DebugLevel, ref this.showDebug, value))
                { this.FilterChanged.Raise(this, EventArgs.Empty); }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to filter by Info logs.
        /// </summary>
        public bool InfoLevel
        {
            get { return this.showInfo; }
            set
            {
                if (this.Set(() => this.InfoLevel, ref this.showInfo, value))
                { this.FilterChanged.Raise(this, EventArgs.Empty); }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to filter by Warn logs.
        /// </summary>
        public bool WarnLevel
        {
            get { return this.showWarn; }
            set
            {
                if (this.Set(() => this.WarnLevel, ref this.showWarn, value))
                { this.FilterChanged.Raise(this, EventArgs.Empty); }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to filter by Debug logs.
        /// </summary>
        public bool ErrorLevel
        {
            get { return this.showError; }
            set
            {
                if (this.Set(() => this.ErrorLevel, ref this.showError, value))
                { this.FilterChanged.Raise(this, EventArgs.Empty); }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to filter by Fatal logs.
        /// </summary>
        public bool FatalLevel
        {
            get { return this.showFatal; }
            set
            {
                if (this.Set(() => this.FatalLevel, ref this.showFatal, value))
                { this.FilterChanged.Raise(this, EventArgs.Empty); }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to filter by Unknown logs.
        /// </summary>
        public bool UnknownLevel
        {
            get { return this.showUnknown; }
            set
            {
                if (this.Set(() => this.UnknownLevel, ref this.showUnknown, value))
                { this.FilterChanged.Raise(this, EventArgs.Empty); }
            }
        }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message
        {
            get { return this.message; }
            set
            {
                if (this.Set(() => this.Message, ref this.message, value))
                { this.FilterChanged.Raise(this, EventArgs.Empty); }
            }
        }

        /// <summary>
        /// Gets or sets the thread.
        /// </summary>
        public string Thread
        {
            get { return this.thread; }
            set
            {
                if (this.Set(() => this.Thread, ref this.thread, value))
                { this.FilterChanged.Raise(this, EventArgs.Empty); }
            }
        }

        /// <summary>
        /// Gets or sets the host.
        /// </summary>
        public string Host
        {
            get { return this.host; }
            set
            {
                if (this.Set(() => this.Host, ref this.host, value))
                { this.FilterChanged.Raise(this, EventArgs.Empty); }
            }
        }

        /// <summary>
        /// Gets or sets the machine.
        /// </summary>
        public string Machine
        {
            get { return this.machine; }
            set
            {
                if (this.Set(() => this.Machine, ref this.machine, value))
                { this.FilterChanged.Raise(this, EventArgs.Empty); }
            }
        }

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        public string User
        {
            get { return this.user; }
            set
            {
                if (this.Set(() => this.User, ref this.user, value))
                { this.FilterChanged.Raise(this, EventArgs.Empty); }
            }
        }

        /// <summary>
        /// Gets or sets the application.
        /// </summary>
        public string Application
        {
            get { return this.application; }
            set
            {
                if (this.Set(() => this.Application, ref this.application, value))
                { this.FilterChanged.Raise(this, EventArgs.Empty); }
            }
        }

        /// <summary>
        /// Gets or sets the class.
        /// </summary>
        public string Class
        {
            get { return this.@class; }
            set
            {
                if (this.Set(() => this.Class, ref this.@class, value))
                { this.FilterChanged.Raise(this, EventArgs.Empty); }
            }
        }

        /// <summary>
        /// Gets or sets the method.
        /// </summary>
        public string Method
        {
            get { return this.method; }
            set
            {
                if (this.Set(() => this.Method, ref this.method, value))
                { this.FilterChanged.Raise(this, EventArgs.Empty); }
            }
        }

        /// <summary>
        /// Gets or sets the code file.
        /// </summary>
        public string CodeFile
        {
            get { return this.codeFile; }
            set
            {
                if (this.Set(() => this.CodeFile, ref this.codeFile, value))
                { this.FilterChanged.Raise(this, EventArgs.Empty); }
            }
        }

        /// <summary>
        /// Gets or sets the logger.
        /// </summary>
        public string Logger
        {
            get { return this.logger; }
            set
            {
                if (this.Set(() => this.Logger, ref this.logger, value))
                { this.FilterChanged.Raise(this, EventArgs.Empty); }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is empty.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return string.IsNullOrWhiteSpace(this.Application) &&
                    string.IsNullOrWhiteSpace(this.Class) &&
                    string.IsNullOrWhiteSpace(this.CodeFile) &&
                    string.IsNullOrWhiteSpace(this.Host) &&
                    string.IsNullOrWhiteSpace(this.Logger) &&
                    string.IsNullOrWhiteSpace(this.Machine) &&
                    string.IsNullOrWhiteSpace(this.Message) &&
                    string.IsNullOrWhiteSpace(this.Method) &&
                    string.IsNullOrWhiteSpace(this.Thread) &&
                    string.IsNullOrWhiteSpace(this.User) &&
                    !this.DebugLevel &&
                    !this.InfoLevel &&
                    !this.WarnLevel &&
                    !this.ErrorLevel &&
                    !this.FatalLevel &&
                    !this.UnknownLevel;
            }
        }

        /// <summary>
        /// Clears this instance.
        /// </summary>
        public void Clear()
        {
            // Clear backing fields (notify prop change event is at bottom of event, prevent repetitive re-calculation of filtered entries)
            this.showDebug = false;
            this.showError = false;
            this.showFatal = false;
            this.showInfo = false;
            this.showUnknown = false;
            this.showWarn = false;
            this.application = string.Empty;
            this.@class = string.Empty;
            this.codeFile = string.Empty;
            this.host = string.Empty;
            this.logger = string.Empty;
            this.machine = string.Empty;
            this.message = string.Empty;
            this.method = string.Empty;
            this.thread = string.Empty;
            this.user = string.Empty;

            // Raise events
            this.RaisePropertyChanged(string.Empty);
            this.FilterCleared.Raise(this, EventArgs.Empty);
        }
    }
}