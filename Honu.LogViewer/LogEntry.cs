﻿using System;

namespace Honu.LogViewer
{
    /// <summary>
    /// Defines basic behavior of a <see cref="ILogEntry"/>
    /// </summary>
    public class LogEntry : ILogEntry
    {
        /// <summary>
        /// Gets or sets the time of the log entry
        /// </summary>
        public DateTime TimeStamp
        { get; protected set; }

        /// <summary>
        /// Gets or sets the severity of the log entry
        /// </summary>
        public LogLevel Level
        { get; protected set; }

        /// <summary>
        /// Gets or sets the thread in which the log entry occurred
        /// </summary>
        public string Thread
        { get; protected set; }

        /// <summary>
        /// Gets or sets the log message
        /// </summary>
        public string Message
        { get; protected set; }

        /// <summary>
        /// Gets or sets the machine identifier
        /// </summary>
        public string Machine
        { get; protected set; }

        /// <summary>
        /// Gets or sets the host identifier
        /// </summary>
        public string Host
        { get; protected set; }

        /// <summary>
        /// Gets or sets the user associated with the log entry
        /// </summary>
        public string User
        { get; protected set; }

        /// <summary>
        /// Gets or sets the application which created the log entry
        /// </summary>
        public string Application
        { get; protected set; }

        /// <summary>
        /// Gets or sets the class which created the log entry
        /// </summary>
        public string Class
        { get; protected set; }

        /// <summary>
        /// Gets or sets the method which created the log entry
        /// </summary>
        public string Method
        { get; protected set; }

        /// <summary>
        /// Gets or sets the line number which logged the event
        /// </summary>
        public string Line
        { get; protected set; }

        /// <summary>
        /// Gets or sets the code file which logged the event
        /// </summary>
        public string CodeFile
        { get; protected set; }

        /// <summary>
        /// Gets or sets the exception message
        /// </summary>
        public string Throwable
        { get; protected set; }

        /// <summary>
        /// Gets or sets the log file path
        /// </summary>
        public string LogFile
        { get; protected set; }

        /// <summary>
        /// Gets or sets the logging class
        /// </summary>
        public string Logger
        { get; protected set; }
    }
}
