﻿using System;

namespace Honu.LogViewer
{
    /// <summary>
    /// Defines the behavior of a single log entry
    /// </summary>
    public interface ILogEntry
    {
        /// <summary>
        /// Gets the time of the log entry
        /// </summary>
        DateTime TimeStamp
        { get; }

        /// <summary>
        /// Gets the severity of the log entry
        /// </summary>
        LogLevel Level
        { get; }

        /// <summary>
        /// Gets the thread in which the log entry occurred
        /// </summary>
        string Thread
        { get; }

        /// <summary>
        /// Gets the log message
        /// </summary>
        string Message
        { get; }

        /// <summary>
        /// Gets the machine identifier
        /// </summary>
        string Machine
        { get; }

        /// <summary>
        /// Gets the host identifier
        /// </summary>
        string Host
        { get; }

        /// <summary>
        /// Gets the user associated with the log entry
        /// </summary>
        string User
        { get; }

        /// <summary>
        /// Gets the application which created the log entry
        /// </summary>
        string Application
        { get; }

        /// <summary>
        /// Gets the class which created the log entry
        /// </summary>
        string Class
        { get; }

        /// <summary>
        /// Gets the method which created the log entry
        /// </summary>
        string Method
        { get; }

        /// <summary>
        /// Gets the line number which logged the event
        /// </summary>
        string Line
        { get; }

        /// <summary>
        /// Gets the code file which logged the event
        /// </summary>
        string CodeFile
        { get; }

        /// <summary>
        /// Gets the exception message
        /// </summary>
        string Throwable
        { get; }

        /// <summary>
        /// Gets the log file path
        /// </summary>
        string LogFile
        { get; }

        /// <summary>
        /// Gets the logging class
        /// </summary>
        string Logger
        { get; }
    }
}