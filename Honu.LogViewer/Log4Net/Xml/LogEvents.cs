﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Honu.LogViewer.Log4Net.Xml
{
    /// <summary>
    /// Log4Net Event
    /// </summary>
    [Serializable]
    [XmlType(TypeName = "events", Namespace = "http://logging.apache.org/log4net/schemas/log4net-events-1.2")]
    [XmlRoot(Namespace = "http://logging.apache.org/log4net/schemas/log4net-events-1.2", IsNullable = false)]
    public sealed class LogEvents
    {
        /// <summary>
        /// Gets or sets the events
        /// </summary>
        [XmlElement("event")]
        public List<LogEvent> Events
        { get; set; }

        /// <summary>
        /// Gets or sets the log version
        /// </summary>
        [XmlAttribute("version")]
        public string Version
        { get; set; }
    }
}