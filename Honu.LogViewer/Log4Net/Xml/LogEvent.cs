﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Serialization;

namespace Honu.LogViewer.Log4Net.Xml
{
    /// <summary>
    /// Log4Net Event
    /// </summary>
    [Serializable]
    [DebuggerDisplay("{Message}")]
    [XmlType(TypeName = "event", Namespace = "http://logging.apache.org/log4net/schemas/log4net-events-1.2")]
    public sealed class LogEvent
    {
        /// <summary>
        /// Gets or sets the event message
        /// </summary>
        [XmlElement("message")]
        public string Message
        { get; set; }

        /// <summary>
        /// Gets or sets the exception of the event
        /// </summary>
        [XmlElement("exception")]
        public string Exception
        { get; set; }

        /// <summary>
        /// Gets or sets the event properties
        /// </summary>
        [XmlArray("properties")]
        [XmlArrayItem("data", typeof(LogEventProperty), IsNullable = false)]
        public List<LogEventProperty> Properties
        { get; set; }

        /// <summary>
        /// Gets or sets the log entry location information
        /// </summary>
        [XmlElement("locationInfo")]
        public LogLocationInfo Location
        { get; set; }

        /// <summary>
        /// Gets or sets the event logger
        /// </summary>
        [XmlAttribute("logger")]
        public string Logger
        { get; set; }

        /// <summary>
        /// Gets or sets the event timestamp
        /// </summary>
        [XmlAttribute("timestamp")]
        public string Timestamp
        { get; set; }

        /// <summary>
        /// Gets or sets the log level
        /// </summary>
        [XmlAttribute("level")]
        public string Level
        { get; set; }

        /// <summary>
        /// Gets or sets the thread in which the event occurred
        /// </summary>
        [XmlAttribute("thread")]
        public string Thread
        { get; set; }

        /// <summary>
        /// Gets or sets the event domain
        /// </summary>
        [XmlAttribute("domain")]
        public string Domain
        { get; set; }

        /// <summary>
        /// Gets or sets the name of the user that prompted the event
        /// </summary>
        [XmlAttribute("username")]
        public string UserName
        { get; set; }
    }
}