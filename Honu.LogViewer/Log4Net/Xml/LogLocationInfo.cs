﻿using System;
using System.Xml.Serialization;

namespace Honu.LogViewer.Log4Net.Xml
{
    /// <summary>
    /// Log4Net Event Location Information
    /// </summary>
    [Serializable]
    [XmlType(TypeName = "locationInfo", Namespace = "http://logging.apache.org/log4net/schemas/log4net-events-1.2")]
    public sealed class LogLocationInfo
    {
        /// <summary>
        /// Gets or sets the class
        /// </summary>
        [XmlAttribute("class")]
        public string Class
        { get; set; }

        /// <summary>
        /// Gets or sets the method
        /// </summary>
        [XmlAttribute("method")]
        public string Method
        { get; set; }

        /// <summary>
        /// Gets or sets the file
        /// </summary>
        [XmlAttribute("file")]
        public string File
        { get; set; }

        /// <summary>
        /// Gets or sets the line
        /// </summary>
        [XmlAttribute("line")]
        public string Line
        { get; set; }
    }
}