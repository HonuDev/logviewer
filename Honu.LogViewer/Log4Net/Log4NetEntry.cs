﻿using System;
using System.Linq;
using Honu.LogViewer.Log4Net.Xml;

namespace Honu.LogViewer.Log4Net
{
    /// <summary>
    /// Log4Net Log Entry
    /// </summary>
    public class Log4NetEntry : LogEntry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Log4NetEntry" /> class
        /// </summary>
        public Log4NetEntry()
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="Log4NetEntry" /> class
        /// </summary>
        /// <param name="logEvent">The log event</param>
        /// <param name="logFilePath">The log file path</param>
        internal Log4NetEntry(LogEvent logEvent, string logFilePath = null)
        {
            // Check for error
            if (logEvent == null)
            { throw new ArgumentNullException("logEvent"); }

            // Get basic log information
            this.Thread = logEvent.Thread;
            this.Message = logEvent.Message;
            this.User = logEvent.UserName;
            this.Throwable = logEvent.Exception;
            this.Logger = logEvent.Logger;
            this.Application = logEvent.Domain;

            // Get log location information
            if (logEvent.Location != null)
            {
                this.Class = logEvent.Location.Class;
                this.CodeFile = logEvent.Location.File;
                this.Line = logEvent.Location.Line;
                this.Method = logEvent.Location.Method;
            }

            // Get host property
            LogEventProperty host = logEvent.Properties.FirstOrDefault(p => p.Name.EndsWith("HostName"));
            if (host != null)
            {
                this.Machine = host.Value;
                this.Host = host.Value;
            }

            // Parse log level
            LogLevel level = LogLevel.Unknown;
            if (Enum.TryParse<LogLevel>(logEvent.Level, true, out level))
            { this.Level = level; }

            // Parse timestamp
            DateTime dt = DateTime.MinValue;
            if (DateTime.TryParse(logEvent.Timestamp, out dt))
            { this.TimeStamp = dt; }

            if (!string.IsNullOrWhiteSpace(logFilePath))
            { this.LogFile = logFilePath; }
        }
    }
}