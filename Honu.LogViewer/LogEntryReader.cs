﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using log4net;

namespace Honu.LogViewer
{
    /// <summary>
    /// Defines the base behavior of the Log Entry Reader class
    /// </summary>
    public abstract class LogEntryReader : ILogEntryReader
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LogEntryReader" /> class.
        /// </summary>
        protected LogEntryReader()
        { this.Logger = LogManager.GetLogger(this.GetType()); }

        /// <summary>
        /// Gets the factory logger
        /// </summary>
        protected ILog Logger
        { get; private set; }

        /// <summary>
        /// Retrieves a collection of <see cref="ILogEntry" />s from the specified file path
        /// </summary>
        /// <param name="filePath">The path of the file to be read</param>
        /// <returns>A collection of <see cref="ILogEntry" />s</returns>
        public IEnumerable<ILogEntry> GetLogs(string filePath)
        {
            // Check for error
            if (string.IsNullOrWhiteSpace(filePath))
            { throw new ArgumentNullException("filePath"); }

            if (!File.Exists(filePath))
            { throw new FileNotFoundException("Could not find log file.", filePath); }

            // Open file and retrieve logs
            this.Logger.DebugFormat("Opening file '{0}'", filePath);
            using (FileStream stream = File.OpenRead(filePath))
            { return this.GetLogs(stream, filePath); }
        }

        /// <summary>
        /// Retrieves a collection of <see cref="ILogEntry"/>s from the specified file path
        /// </summary>
        /// <param name="filePath">The path of the file to be read</param>
        /// <returns>A collection of <see cref="ILogEntry"/>s</returns>
        public Task<IEnumerable<ILogEntry>> GetLogsAsync(string filePath)
        {
            // Check for error
            if (string.IsNullOrWhiteSpace(filePath))
            { throw new ArgumentNullException("filePath"); }

            if (!File.Exists(filePath))
            { throw new FileNotFoundException("Could not find log file.", filePath); }

            // Open file and retrieve logs
            this.Logger.DebugFormat("Opening file '{0}'", filePath);
            using (FileStream stream = File.OpenRead(filePath))
            { return this.GetLogsAsync(stream, filePath); }
        }

        /// <summary>
        /// Retrieves a collection of <see cref="ILogEntry" />s from the specified stream
        /// </summary>
        /// <param name="stream">The <see cref="Stream" /> to be read</param>
        /// <returns>A collection of <see cref="ILogEntry" />s</returns>
        public IEnumerable<ILogEntry> GetLogs(Stream stream)
        {
            // Check for error
            if (stream == null)
            { throw new ArgumentNullException("stream"); }

            // Retrieve logs
            return this.GetLogs(stream, string.Empty);
        }

        /// <summary>
        /// Retrieves a collection of <see cref="ILogEntry"/>s from the specified stream
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to be read</param>
        /// <returns>A collection of <see cref="ILogEntry"/>s</returns>
        public Task<IEnumerable<ILogEntry>> GetLogsAsync(Stream stream)
        {
            // Check for error
            if (stream == null)
            { throw new ArgumentNullException("stream"); }

            // Retrieve logs
            return this.GetLogsAsync(stream, string.Empty);
        }

        /// <summary>
        /// Retrieves a collection of <see cref="ILogEntry" />s from the specified stream
        /// </summary>
        /// <param name="stream">The <see cref="Stream" /> to be read</param>
        /// <param name="filePath">The name of the file associated with the <see cref="Stream"/></param>
        /// <returns>A collection of <see cref="ILogEntry" />s</returns>
        public abstract IEnumerable<ILogEntry> GetLogs(Stream stream, string filePath);

        /// <summary>
        /// Retrieves a collection of <see cref="ILogEntry" />s from the specified stream
        /// </summary>
        /// <param name="stream">The <see cref="Stream" /> to be read</param>
        /// <param name="filePath">The name of the file associated with the <see cref="Stream"/></param>
        /// <returns>A collection of <see cref="ILogEntry" />s</returns>
        public abstract Task<IEnumerable<ILogEntry>> GetLogsAsync(Stream stream, string filePath);
    }
}