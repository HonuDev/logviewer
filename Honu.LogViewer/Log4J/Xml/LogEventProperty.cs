﻿using System;
using System.Diagnostics;
using System.Xml.Serialization;

namespace Honu.LogViewer.Log4J.Xml
{
    /// <summary>
    /// Log4J Event Property
    /// </summary>
    [Serializable]
    [DebuggerDisplay("{Name} - {Value}")]
    [XmlType(TypeName = "data", Namespace = "http://logging.apache.org/log4net/schemas/log4j-events")]
    public class LogEventProperty
    {
        /// <summary>
        /// Gets or sets the property name
        /// </summary>
        [XmlAttribute("name")]
        public string Name
        { get; set; }

        /// <summary>
        /// Gets or sets the property value
        /// </summary>
        [XmlAttribute("value")]
        public string Value
        { get; set; }
    }
}