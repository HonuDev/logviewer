﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Honu.LogViewer.Log4J.Xml
{
    /// <summary>
    /// Log4J Event
    /// </summary>
    [Serializable]
    [XmlType(TypeName = "events", Namespace = "http://logging.apache.org/log4net/schemas/log4j-events")]
    [XmlRoot(Namespace = "http://logging.apache.org/log4net/schemas/log4j-events", IsNullable = false)]
    public class LogEvents
    {
        /// <summary>
        /// Gets or sets the events
        /// </summary>
        [XmlElement("event")]
        public List<LogEvent> Events
        { get; set; }
    }
}