﻿using System;
using System.Collections.Generic;
using System.Linq;
using Honu.LogViewer.Log4J.Xml;

namespace Honu.LogViewer.Log4J
{
    /// <summary>
    /// Log4J Entry
    /// </summary>
    public class Log4JEntry : LogEntry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Log4JEntry" /> class.
        /// </summary>
        public Log4JEntry()
        { this.TimeStamp = new DateTime(1970, 1, 1, 0, 0, 0, 0); }

        /// <summary>
        /// Initializes a new instance of the <see cref="Log4JEntry" /> class
        /// </summary>
        /// <param name="logEvent">The log event</param>
        /// <param name="logFilePath">The log file path</param>
        internal Log4JEntry(LogEvent logEvent, string logFilePath = null)
        {
            // Check for error
            if (logEvent == null)
            { throw new ArgumentNullException("logEvent"); }

            // Get basic log information
            this.Thread = logEvent.Thread;
            this.Message = logEvent.Message;
            this.Logger = logEvent.Logger;
            this.Throwable = logEvent.Throwable;

            // Get log location information
            if (logEvent.Location != null)
            {
                this.Class = logEvent.Location.Class;
                this.CodeFile = logEvent.Location.File;
                this.Line = logEvent.Location.Line;
                this.Method = logEvent.Location.Method;
            }

            // Get host property
            LogEventProperty host = logEvent.Properties.FirstOrDefault(p => p.Name.EndsWith("HostName"));
            if (host != null)
            { this.Host = host.Value; }

            // Get property values
            this.Machine = GetPropertyValue(logEvent.Properties, "log4jmachinename");
            this.Application = GetPropertyValue(logEvent.Properties, "log4japp");
            this.User = GetPropertyValue(logEvent.Properties, "UserName");
            this.Host = GetPropertyValue(logEvent.Properties, "HostName");

            // Parse log level
            LogLevel level = LogLevel.Unknown;
            if (Enum.TryParse<LogLevel>(logEvent.Level, true, out level))
            { this.Level = level; }

            // Parse timestamp
            double ms = Convert.ToDouble(logEvent.Timestamp);
            this.TimeStamp = this.TimeStamp.AddMilliseconds(ms).ToLocalTime();

            if (!string.IsNullOrWhiteSpace(logFilePath))
            { this.LogFile = logFilePath; }
        }

        /// <summary>
        /// Gets the value of the specified property
        /// </summary>
        /// <param name="properties">Collection of properties</param>
        /// <param name="propertyName">Property name</param>
        /// <returns>Value of property</returns>
        private static string GetPropertyValue(IEnumerable<LogEventProperty> properties, string propertyName)
        {
            if (properties == null || string.IsNullOrWhiteSpace(propertyName))
            { return string.Empty; }

            LogEventProperty property = properties.FirstOrDefault(p => p.Name.EndsWith(propertyName));
            return property == null ? string.Empty : property.Value;
        }
    }
}