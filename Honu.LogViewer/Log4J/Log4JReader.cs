﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Honu.LogViewer.Log4J.Xml;
using Honu.LogViewer.Properties;

namespace Honu.LogViewer.Log4J
{
    /// <summary>
    /// Log4J Log Entry Reader
    /// </summary>
    public sealed class Log4JReader : LogEntryReader
    {
        /// <summary>
        /// Retrieves a collection of <see cref="ILogEntry" />s from the specified stream
        /// </summary>
        /// <param name="stream">The <see cref="Stream" /> to be read</param>
        /// <param name="filePath">The name of the file associated with the <see cref="Stream"/></param>
        /// <returns>A collection of <see cref="ILogEntry" />s</returns>
        public override IEnumerable<ILogEntry> GetLogs(Stream stream, string filePath)
        {
            // Check for error
            if (stream == null)
            { throw new ArgumentNullException("stream"); }

            StreamReader reader = new StreamReader(stream);
            string text = string.Concat(Resources.Log4JXmlLayoutFront, reader.ReadToEnd(), Resources.Log4JXmlLayoutBack);
            XmlSerializer serializer = new XmlSerializer(typeof(LogEvents));

            using (StringReader stringReader = new StringReader(text))
            {
                LogEvents ev = serializer.Deserialize(stringReader) as LogEvents;
                foreach (LogEvent @event in ev.Events)
                { yield return new Log4JEntry(@event, filePath); }
            }
        }

        /// <summary>
        /// Retrieves a collection of <see cref="ILogEntry" />s from the specified stream
        /// </summary>
        /// <param name="stream">The <see cref="Stream" /> to be read</param>
        /// <param name="filePath">The name of the file associated with the <see cref="Stream"/></param>
        /// <returns>A collection of <see cref="ILogEntry" />s</returns>
        public override async Task<IEnumerable<ILogEntry>> GetLogsAsync(Stream stream, string filePath)
        {
            if (stream == null)
            { return null; }

            StreamReader reader = new StreamReader(stream);
            string text = await reader.ReadToEndAsync();
            text = string.Concat(Resources.Log4JXmlLayoutFront, text, Resources.Log4JXmlLayoutBack);

            return await Task.Run<IEnumerable<ILogEntry>>(() =>
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(LogEvents));
                    using (StringReader stringReader = new StringReader(text))
                    {
                        LogEvents ev = serializer.Deserialize(stringReader) as LogEvents;
                        if (ev == null || ev.Events == null)
                        { return null; }

                        return ev.Events.Select(e => new Log4JEntry(e, filePath)).Cast<ILogEntry>();
                    }
                });
        }
    }
}