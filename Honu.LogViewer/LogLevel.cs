﻿using System.Diagnostics.CodeAnalysis;

namespace Honu.LogViewer
{
    /// <summary>
    /// Identifies the log severity
    /// </summary>
    [SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1630:DocumentationTextMustContainWhitespace", Justification = "Reviewed.")]
    public enum LogLevel : int
    {
        /// <summary>
        /// Unknown or Custom log level
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Debug (Verbose)
        /// </summary>
        Debug = 1,

        /// <summary>
        /// Informative
        /// </summary>
        Info = 2,

        /// <summary>
        /// Warning
        /// </summary>
        Warn = 3,

        /// <summary>
        /// Error
        /// </summary>
        Error = 4,

        /// <summary>
        /// Fatal
        /// </summary>
        Fatal = 5
    }
}