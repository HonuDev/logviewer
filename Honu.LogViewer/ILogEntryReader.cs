﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Honu.LogViewer
{
    /// <summary>
    /// Defines the behavior of a <see cref="ILogEntry"/> reader
    /// </summary>
    public interface ILogEntryReader
    {
        /// <summary>
        /// Retrieves a collection of <see cref="ILogEntry"/>s from the specified file path
        /// </summary>
        /// <param name="filePath">The path of the file to be read</param>
        /// <returns>A collection of <see cref="ILogEntry"/>s</returns>
        IEnumerable<ILogEntry> GetLogs(string filePath);

        /// <summary>
        /// Retrieves a collection of <see cref="ILogEntry"/>s from the specified file path
        /// </summary>
        /// <param name="filePath">The path of the file to be read</param>
        /// <returns>A collection of <see cref="ILogEntry"/>s</returns>
        Task<IEnumerable<ILogEntry>> GetLogsAsync(string filePath);

        /// <summary>
        /// Retrieves a collection of <see cref="ILogEntry"/>s from the specified stream
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to be read</param>
        /// <returns>A collection of <see cref="ILogEntry"/>s</returns>
        IEnumerable<ILogEntry> GetLogs(Stream stream);

        /// <summary>
        /// Retrieves a collection of <see cref="ILogEntry"/>s from the specified stream
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to be read</param>
        /// <returns>A collection of <see cref="ILogEntry"/>s</returns>
        Task<IEnumerable<ILogEntry>> GetLogsAsync(Stream stream);

        /// <summary>
        /// Retrieves a collection of <see cref="ILogEntry" />s from the specified stream
        /// </summary>
        /// <param name="stream">The <see cref="Stream" /> to be read</param>
        /// <param name="filePath">The name of the file associated with the <see cref="Stream"/></param>
        /// <returns>A collection of <see cref="ILogEntry" />s</returns>
        IEnumerable<ILogEntry> GetLogs(Stream stream, string filePath);

        /// <summary>
        /// Retrieves a collection of <see cref="ILogEntry" />s from the specified stream
        /// </summary>
        /// <param name="stream">The <see cref="Stream" /> to be read</param>
        /// <param name="filePath">The name of the file associated with the <see cref="Stream"/></param>
        /// <returns>A collection of <see cref="ILogEntry" />s</returns>
        Task<IEnumerable<ILogEntry>> GetLogsAsync(Stream stream, string filePath);
    }
}