##Honu LogViewer##

Log4Net / Log4J Log viewing application.

Written using C#, WPF, [MVVM Light Toolkit](https://mvvmlight.codeplex.com/). Uses the .NET Framework 4.5 and Task-based Asynchronous Pattern (TAP).

* Smooth, responsive UI tested with log files with over 30k log entries.
* Load multiple files in one go
* Includes recently-opened file list
* Drag and drop support
* Quick filtering by message and Logging Level on the main window
* Advanced filtering provided
* Color-coding is user-customizable
* Selected log entry is presented in an easy-to-understand panel

Project primarily based on the *C# WPF Log4Net Viewer* on CodeProject, located [here](http://www.codeproject.com/Articles/30795/C-WPF-Log-Net-Viewer). Supports log files of both *XmlLayoutSchemaLog4j* and *XMLLayout* formats.